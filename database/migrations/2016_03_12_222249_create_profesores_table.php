<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ci')->unique();
            $table->string('primer_nombre');
            $table->string('segundo_nombre');
            $table->string('tercer_nombre')->nulleable();
            $table->string('primer_apellido');
            $table->string('segundo_apellido')->nulleable();
            $table->enum('sexo', ['Masculino', 'Femenino']);
            $table->text('direccion');
            $table->date('fecha_nacimiento');
            $table->string('telefono');
            $table->string('email')->unique();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profesores');
    }
}
