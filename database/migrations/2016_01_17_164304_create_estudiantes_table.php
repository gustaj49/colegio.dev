<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ci')->unique();
            $table->string('primer_nombre');
            $table->string('segundo_nombre');
            $table->string('tercer_nombre')->nulleable();
            $table->string('primer_apellido');
            $table->string('segundo_apellido')->nulleable();
            $table->enum('sexo', ['Masculino', 'Femenino']);
            $table->date('fecha_nacimiento');
            $table->text('observacion');
            $table->text('direccion');
            $table->smallInteger('anio_egreso')->unsigned()->nulleable();
            $table->integer('ciudad_id')->unsigned();
            $table->integer('periodo_id')->unsigned();
            $table->integer('seccion_id')->unsigned()->nulleable();
            $table->integer('representante_id')->unsigned();
            $table->timestamps();

            $table->foreign('seccion_id')->references('id')
                                        ->on('secciones')
                                        ->onUpdate('cascade');

            $table->foreign('ciudad_id')->references('id')
                                        ->on('ciudades')
                                        ->onUpdate('cascade')
                                        ->onDelete('cascade');

            $table->foreign('representante_id')->references('id')
                                            ->on('representantes')
                                            ->onUpdate('cascade')
                                            ->onDelete('cascade');

            $table->foreign('periodo_id')->references('id')
                                        ->on('periodos')
                                        ->onUpdate('cascade');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estudiantes');
    }
}
