<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesoresMateriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesores_materias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('materia_id')->unsigned();
            $table->integer('profesor_id')->unsigned();

            $table->foreign('materia_id')->references('id')
                                        ->on('materias')
                                        ->onUpdate('cascade')
                                        ->onDelete('cascade');

            $table->foreign('profesor_id')->references('id')
                                        ->on('profesores')
                                        ->onUpdate('cascade')
                                        ->onDelete('cascade');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profesores_materias');
    }
}
