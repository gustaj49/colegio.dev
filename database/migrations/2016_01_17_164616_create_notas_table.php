<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estudiante_id')->unsigned();
            $table->integer('materia_id')->unsigned();
            $table->integer('seccion_id')->unsigned();
            $table->integer('periodo_id')->unsigned();
            // $table->string('periodo')->unique();
            // $table->timestamps();

            $table->foreign('estudiante_id')->references('id')
                                            ->on('estudiantes')
                                            ->onUpdate('cascade');

            $table->foreign('seccion_id')->references('id')
                                        ->on('secciones')
                                        ->onUpdate('cascade');

            $table->foreign('materia_id')->references('id')
                                        ->on('materias')
                                        ->onUpdate('cascade');

            $table->foreign('periodo_id')->references('id')
                                        ->on('periodos')
                                        ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notas');
    }
}
