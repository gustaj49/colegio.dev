<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(RepresentantesTableSeeder::class);
        $this->call(VenezuelaTableSeeder::class);
        // $this->call(EstudiantesTableSeeder::class);
        $this->call(PeriodoTableSeeder::class);
        $this->call(SeccionesTableSeeder::class);
        $this->call(ProfesoresTableSeeder::class);
    }
}
