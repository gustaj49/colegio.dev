<?php

use Illuminate\Database\Seeder;

class SeccionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $max = 30;
        for($i = 1; $i <= 5; $i++)
        {
	        \DB::table('secciones')->insert([
	        	['nombre' => 'A', 'grado' => $i, 'max' => $max],
	        	['nombre' => 'B', 'grado' => $i, 'max' => $max],
	        	['nombre' => 'C', 'grado' => $i, 'max' => $max],
	        	['nombre' => 'D', 'grado' => $i, 'max' => $max]
	        ]);
	    }
    }
}
