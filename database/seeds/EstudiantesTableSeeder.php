<?php

use Illuminate\Database\Seeder;

class EstudiantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $table = \DB::table('secciones');
        $secciones = \App\Seccion::all();
        $representantes = \App\Representante::all();

        foreach($secciones as $seccion)
        {
        	for($i = $seccion->estudiantes->count(); $i < $seccion->max; $i++)
        	{
        		\App\Estudiante::create([
                    'ci' => $faker->numberBetween(25000000, 27000000),
        			'primer_nombre' => $faker->firstName,
        			'segundo_nombre' => $faker->firstName,
        			'tercer_nombre' => $faker->firstName,
        			'primer_apellido' => $faker->lastName,
        			'segundo_apellido' => $faker->lastName,
                    'sexo' => $faker->numberBetween(1, 2),
                    'periodo_id' => 1,
        			'direccion' => $faker->address,
        			'fecha_nacimiento' => $faker->date('Y-m-d', $max = 'now'),
        			'ciudad_id' => $faker->randomElement(
        				\DB::table('ciudades')->select('id')->get())->id,
                    'seccion_id' => $seccion->id,
                    'representante_id' => $faker->numberBetween(1, $representantes->count())
        		]);
        	}
        }
    }
}
