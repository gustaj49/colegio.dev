<?php

use Illuminate\Database\Seeder;

class PeriodoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Periodo::create([
        	'periodo' => '2015/2016',
        	'inicio' => '2015-09-7',
        	'fin' => '2016-06-29'
        ]);
    }
}
