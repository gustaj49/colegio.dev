<?php

use Illuminate\Database\Seeder;

class ProfesoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = \Faker\Factory::create();
        for($i = 0; $i < 25; $i++)
    	{
    		\App\Profesor::create([
                'ci' => $faker->numberBetween(18000000, 20000000),
    			'primer_nombre' => $faker->firstName,
    			'segundo_nombre' => $faker->firstName,
    			'tercer_nombre' => $faker->firstName,
    			'primer_apellido' => $faker->lastName,
    			'segundo_apellido' => $faker->lastName,
                'sexo' => $faker->numberBetween(1, 2),
    			'direccion' => $faker->address,
    			'fecha_nacimiento' => $faker->date('Y-m-d', $max = 'now'),
    			'telefono' => $faker->numberBetween(4240000000, 4270000000),
    			'email' => $faker->safeEmail
    		]);
    	}
    }
}
