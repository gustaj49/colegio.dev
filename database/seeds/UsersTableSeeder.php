<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = \Faker\Factory::create();
        
        for($i = 0; $i < 30; $i++)
        {
            \DB::table('usuarios')->insert([
    	    	'username' => $faker->userName,
    	    	'email' => $faker->safeEmail,
    	    	'password' => \Hash::make('batman')
       		]);
        }

    }
}
