@extends('layouts.main')

@section('head_content')
	<div class="page-header">
		<h1><span class="glyphicon glyphicon-pencil text-warning"></span> {{ $estudiante->ci }}, {{ $estudiante->primer_nombre }} {{ $estudiante->primer_apellido }}</h1>
	</div>
@endsection

@section('content')
<div class="">

	<!-- Nav-tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li class="active" role="presentation">
			<a href="#estudiante-tab" data-toggle="tab" role="tab">Estudiante</a>
		</li>

		<li role="presentation">
			<a href="#representante-tab" data-toggle="tab" role="tab">Representante</a>
		</li>

		<li role="presentation">
			<a href="#academico-tab" data-toggle="tab" role="tab">Académico</a>
		</li>
	</ul>

	<!-- Content-tabs -->
	<div class="tab-content" style="padding: 15px">

		<div id="estudiante-tab" class="tab-pane active" role="tabpanel">
			<form id="estudiante_form" class="form-horizontal form_update" action="{{ action('EstudianteController@update') }}" method="post">

				{!! method_field('PUT') !!}
				{!! csrf_field() !!}

				<input type="hidden" name="option" value="estudiante"></input>
				<input type="hidden" name="id" value="{{ $estudiante->id }}"></input>

				<div class="">
					<div class="form-group">
						<label for="ci" class="control-label  col-md-2">C.I.</label>
						<div class="col-md-4">
							<input id="ci" class="form-control" type="number" name="ci" value="{{ $estudiante->ci }}" required>
						</div>

						<label class="control-label col-md-2">Sexo: </label>

						<div class="col-md-4">
							<label class="radio-inline">
							@if ($estudiante->sexo == 'Masculino')
								<input type="radio" name="sexo" value="Masculino" checked="checked">Masculino
							@else
								<input type="radio" name="sexo" value="Masculino">Masculino
							@endif
							</label>
							<label class="radio-inline">
							@if ($estudiante->sexo == 'Femenino')
								<input type="radio" name="sexo" value="Femenino" checked="checked">Femenino
							@else
								<input type="radio" name="sexo" value="Femenino">Femenino
							@endif
							</label>
						</div>
					</div>

					<div class="form-group">


						<label class="control-label col-md-2">Nombres</label>
						<div class="">
							<div class="col-md-3">
								<input id="primer_nombre" class="form-control" type="text" name="primer_nombre" value="{{ $estudiante->primer_nombre }}" required>
							</div>
							<div class="col-md-3">
								<input id="segundo_nombre" class="form-control" type="text" name="segundo_nombre" value="{{ $estudiante->segundo_nombre }}" required>
							</div>
							<div class="col-md-3">
								<input id="tercer_nombre" class="form-control" type="text" name="tercer_nombre" value="{{ $estudiante->tercer_nombre }}">
							</div>
						</div>
					</div>

					<div class="form-group">
						{{-- {!! Form::label(NULL, 'Apellidos', ['class' => 'control-label col-md-2']) !!} --}}
						<label class="control-label col-md-2">Apellidos</label>
						<div class="">
							<div class="col-md-5">
								<input id="primer_apellido" class="form-control" type="text" name="primer_apellido" value="{{ $estudiante->primer_apellido }}" required>
							</div>
							<div class="col-md-4">
								<input id="segundo_apellido" class="form-control" type="text" name="segundo_apellido" value="{{ $estudiante->segundo_apellido }}">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="fecha_nacimiento" class="control-label col-md-2">Fecha de nacimiento</label>
						<div class="">
							<div class="col-md-6">
								<input id="fecha_nacimiento" type="date" name="fecha_nacimiento" class="form-control" value="{{ $estudiante->fecha_nacimiento }}" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="observacion" class="control-label col-md-2">Observación</label>
						<div class="">
							<div class="col-md-9">
								<textarea id="observacion" class="form-control" name="observacion" rows="3">{{ $estudiante->observacion }}</textarea>
							</div>
						</div>
					</div>

					<div class="form-group">
						{{-- {!! Form::label('direccion', 'Dirección', ['class' => 'control-label col-md-2']) !!} --}}
						<label for="direccion" class="control-label col-md-2">Dirección</label>
						<div class="">
							<div class="col-md-9">
								{{-- {!! Form::textarea('direccion', NULL, ['class' => 'form-control', 'rows' => 3]) !!} --}}
								<textarea id="direccion" class="form-control" name="direccion" rows="3" required>{{ $estudiante->direccion }}</textarea>
							</div>
						</div>
					</div>

					<div class="form-group">
						{{-- {!! Form::label(NULL, 'Lugar de nacimiento', ['class' => 'control-label col-md-2']) !!} --}}
						<label class="control-label col-md-2">Lugar de nacimiento</label>
						<div class="col-md-4">
							{{-- {!! Form::select('estado', $estados, NULL, ['class' => 'form-control']) !!} --}}
							<select id="estado" class="form-control" required>
								@foreach($estados as $key => $value)
									@if ($key == $estudiante->ciudad->estado_id)
										<option value="{{ $key }}" selected="selected">{{ $value }}</option>
									@else
										<option value="{{ $key }}">{{ $value }}</option>
									@endif
								@endforeach
							</select>
						</div>
						<div class="col-md-4">
							{{-- {!! Form::select('ciudad', ['1', '2'], NULL, ['class' => 'form-control']) !!} --}}
							<select id="ciudad" class="form-control" name="ciudad_id" required>
								@foreach($ciudades as $key => $value)
									@if ($key == $estudiante->ciudad_id)
										<option value="{{ $key }}" selected="selected">{{ $value }}</option>
									@else
										<option value="{{ $key }}">{{ $value }}</option>
									@endif
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="form-group btn-group-center">
					<button type="submit" class="btn btn-primary">Actualizar</button>
					<button type="reset" class="btn btn-default">Restablecer</button>
				</div>
			</form>
		</div>

		<div id="representante-tab" class="tab-pane" role="tabpanel">
			<form id="representante_form" class="form-horizontal form_update" action="{{ action('EstudianteController@update') }}" method="post">

				{!! method_field('PUT') !!}
				{!! csrf_field() !!}
				<input type="hidden" name="option" value="representante"></input>
				<input type="hidden" name="ci" value="{{ $estudiante->ci }}"></input>

				<div class="form-group">
					<label for="rep_ci" class="control-label col-md-2">C.I.</label>
					<div class="col-md-4">
						<input id="rep_ci" class="form-control" type="number" name="rep_ci" value="{{ $representante->ci }}" required>
					</div>

					<label class="control-label col-md-2">Sexo: </label>
					<div class="col-md-4">
						<label class="radio-inline">
						@if ($representante->sexo == 'Masculino')
							<input type="radio" name="rep_sexo" value="Masculino" checked="checked">Masculino
						@else
							<input type="radio" name="rep_sexo" value="Masculino">Masculino
						@endif
						</label>

						<label class="radio-inline">
						@if ($representante->sexo == 'Femenino')
							<input type="radio" name="rep_sexo" value="Femenino" checked="checked"> Femenino
						@else
							<input type="radio" name="rep_sexo" value="Femenino"> Femenino
						@endif
						</label>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Nombres</label>
					<div class="col-md-3">
						<input id="rep_primer_nombre" class="form-control" type="text" name="rep_primer_nombre" value="{{ $representante->primer_nombre }}" required>
					</div>
					<div class="col-md-3">
						<input id="rep_segundo_nombre" class="form-control" type="text" name="rep_segundo_nombre" value="{{ $representante->segundo_nombre }}" required>
					</div>
					<div class="col-md-3">
						<input id="rep_tercer_nombre" class="form-control" type="text" name="rep_tercer_nombre" value="{{ $representante->tercer_nombre }}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Apellidos</label>
					<div class="col-md-4">
						<input id="rep_primer_apellido" class="form-control" type="text" name="rep_primer_apellido" value="{{ $representante->primer_apellido }}" required>
					</div>
					<div class="col-md-4">
						<input id="rep_segundo_apellido" class="form-control" type="text" name="rep_segundo_apellido" value="{{ $representante->segundo_apellido }}">
					</div>
				</div>

				<div class="form-group">
					<label for="rep_fecha_nacimiento" class="control-label col-md-2">Fecha de nacimiento</label>
					<div class="col-md-6">
						<input id="rep_fecha_nacimiento" class="form-control" type="date" name="rep_fecha_nacimiento" value="{{ $representante->fecha_nacimiento }}" required>
					</div>
				</div>

				<div class="form-group">
					<label for="rep_direccion" class="control-label col-md-2">Dirección</label>
					<div class="col-md-9">
						<textarea id="rep_direccion" class="form-control" name="rep_direccion" rows="3" required>{{ $representante->direccion }}</textarea>
					</div>
				</div>

				<div class="form-group">
					<label for="rep_telefono" class="control-label col-md-2">Teléfono</label>
					<div class="col-md-3">
						<input id="rep_telefono" class="form-control" type="number" name="rep_telefono" value="{{ $representante->telefono }}" required>
					</div>

					<label for="rep_telefono_trabajo" class="control-label col-md-2">Trabajo</label>
					<div class="col-md-3">
						<input id="rep_telefono_trabajo" class="form-control" type="number" name="rep_telefono_trabajo" value="{{ $representante->telefono_trabajo }}">
					</div>
				</div>

				<div class="form-group">
					<label for="rep_trabajo" class="control-label col-md-2">Trabajo</label>
					<div class="col-md-6">
						<input id="rep_trabajo" class="form-control col-md-2" type="text" name="rep_trabajo" value="{{ $representante->trabajo }}">
					</div>
				</div>

				<div class="form-group btn-group-center">
					<button type="submit" class="btn btn-primary">Actualizar</button>
					<button type="reset" class="btn btn-default">Restablecer</button>
				</div>
			</form>
		</div>

		<div id="academico-tab" class="tab-pane" role="tabpanel">
			<form id="academico_form" class="form-horizontal form_update" action="{{ action('EstudianteController@update') }}" method="post">

				{!! method_field('PUT') !!}
				{!! csrf_field() !!}

				<input type="hidden" name="option" value="academico"></input>
				<input type="hidden" name="ci" value="{{ $estudiante->ci }}"></input>

				<div class="form-group">
					<label class="control-label col-md-2" for="grado">Grado</label>
					<div class="col-md-4">
						<select id="grado" class="form-control grado-select" name="grado" required>
							@foreach($grados as $key => $value)
							@if ($key === $estudiante->seccion->grado)
								<option value="{{ $key }} " selected="selected">{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
							@endforeach
						</select>
					</div>

					<label class="control-label col-md-2" for="seccion">Sección</label>
					<div class="col-md-4">
						<select id="seccion" class="form-control seccion-select" name="seccion" required>
							@foreach ($secciones as $key => $value)
								@if ($estudiante->seccion_id === $key)
									<option value="{{ $key }}" selected="selected">{{ $value }}</option>
								@else
									<option value="{{ $key }}">{{ $value }}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="periodo" class="control-label col-md-2">Periodo actual</label>
					<div class="col-md-4">
						<select class="form-control" name="periodo" required>
							@foreach($periodos as $key => $value)
								@if($estudiante->periodo->id === $key)
									<option value="{{ $key }}" selected="selected">{{ $value }}</option>
								@else
									<option value="{{ $key }}">{{ $value }}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group btn-group-center">
					<button type="submit" class="btn btn-primary">Actualizar</button>
					<button type="reset" class="btn btn-default">Restablecer</button>
				</div>
			</form>
		</div>


	</div>

<!-- 	<div class="form-group btn-group-center" style="margin-top: 15px;">
		<button type="submit" class="btn btn-primary">Actualizar todo</button>
		<button type="reset" class="btn btn-default">Restablecer todo</button>
	</div> -->

</div>
@endsection

@section('script')

<script src="{{ asset('assets/js/change_estado.js') }}"></script>
<script>
	$(document).ready(function() {

/*		$(document).on('submit', '.form_update', function(evento) {
			evento.preventDefault();
			var form = $(this).serializeArray(),
				url = $(this).attr('action');

			$.ajax({
				'data': form,
				'url': url,
				'type': 'post',
				'dataType': 'json',
				'success': function(response) {
					if(response.status) {
						alertify.success(response.msg);
					}
				}
			});

			// $.post(url, form)
			// .done(function(response) {
			// 	console.log(response)
			// });
		});
*/
	});
</script>
@endsection
