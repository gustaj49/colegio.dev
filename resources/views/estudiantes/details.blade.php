@extends('estudiantes.index')
@section('result')

{{-- {{ dd($estudiantes) }} --}}

@if(isset($estudiantes) && !(empty($estudiantes)))
	@foreach($estudiantes as $estudiante)
		<p>Nombre: {{ $estudiante->primer_nombre }}</p>
		<p>CI: {{ $estudiante->ci }}</p>
		<p>Sección: {{ $estudiante->seccion->nombre }}</p>
	@endforeach
@else
	<p>Estudiante no encontrado</p>
@endif

@endsection