@extends('layouts.main')

@section('custom_css')
	<link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
	<div class="page-header">
		<h1><span class="glyphicon glyphicon-search text-primary"></span> Búsqueda de estudiantes</h1>
	</div>
@endsection

@section('content')
<style>
	.buttons {
		margin-top: 25px;
		text-align: center;
	}
	.fade {
	   opacity: 3;
	   -webkit-transition: opacity 0.1s linear;
	      -moz-transition: opacity 0.1s linear;
	       -ms-transition: opacity 0.1s linear;
	        -o-transition: opacity 0.1s linear;
	           transition: opacity 0.1s linear;
 	}
</style>

<div class="">
	{{-- Nav tabs --}}
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active">
			<a role="tab" href="#ci_tab" data-toggle="tab">Cédula de identidad</a>
		</li>
		<li role="presentation">
			<a role="tab" href="#seccion_tab" data-toggle="tab">Secciones</a>
		</li>
	</ul>

	{{-- Content tabs --}}
	<div class="tab-content">

		{{-- Panel de búsqueda CI --}}
		<div role="tabpanel" class="tab-pane active container-tab" id="ci_tab">
			{{-- Búsqueda por cédula de identidad --}}
			<div class="">
				{{-- {!! Form::open(['action' => 'EstudianteController@search', 'method' => 'post','id' => 'form_ci', 'class' => 'form-horizontal']) !!} --}}
				<form id="form_ci" class="form-horizontal" action="{{ action('EstudianteController@search') }}" method="post">

					{{-- {{ Form::hidden('option', 'ci') }} --}}
					{!! csrf_field() !!}
					<input type="hidden" name="option" value="ci">
					<div class="form-group">
						<div class="col-md-2">
							{{-- {{ Form::label('ci', 'Cédula de identidad', ['class' => 'control-label']) }} --}}
							<label for="ci_search" class="control-label">Cédula de identidad</label>
						</div>
						<div class="col-md-4">
							{{-- {{ Form::number('ci_search', NULL, ['class' => 'form-control']) }} --}}
							<input id="ci_search" class="form-control" type="number" name="ci_search">
						</div>

						<div class="col-md-3">
						{{-- {{ Form::submit('Buscar', ['class' => 'btn btn-success']) }} --}}
							<button id="btn_ci" class="btn btn-success" type="submit" name="option" value="ci">Buscar</button>
							<button type="reset" class="btn btn-default" name="button">Restablecer</button>
							{{-- {{ Form::reset('Restablecer', ['class' => 'btn btn-default']) }} --}}
						</div>
					</div>
				</form>
			</div>
		</div>

		{{-- Panel de búsqueda por sección --}}
		<div role="tabpanel" class="tab-pane container-tab" id="seccion_tab">
			{{-- Búsqueda por secciones --}}
			<div class="">
				{{-- {!! Form::open(['action' => 'EstudianteController@search','method' => 'post','id' => 'form_seccion', 'class' => 'form-horizontal']) !!} --}}
				<form id="form_seccion" class="form-horizontal" action="" method="post">
					{{-- {{ Form::hidden('option', 'seccion') }} --}}
					{!! csrf_field() !!}
					<input type="hidden" name="option" value="seccion">
					<div class="form-group">
						<div class="col-md-1">
							{{-- {{ Form::label('grado', 'Grado', ['class' => 'control-label']) }} --}}
							<label for="grado_search" class="control-label">Grado</label>
						</div>

						<div class="col-md-2">
							<select id="grado_search" class="form-control grado-select" name="grado_search">
								<option value="" disabled="disabled" selected="selected">Grado</option>
								@foreach($grados as $key => $value)
									<option value="{{$key}}">{{$value}}</option>
								@endforeach
							</select>
						</div>

						<div class="col-md-1">
							{{-- {{ Form::label('seccion_input', 'Secciones', ['class' => 'control-label']) }} --}}
							<label for="seccion_input">Secciones</label>
						</div>

						<div class="col-md-2">
							{{-- {{ Form::select('seccion_input', [], NULL, ['class' => 'form-control']) }} --}}
							<select id="seccion_input" class="form-control seccion-select" name="seccion_input">
							</select>
						</div>

						<div class="col-md-3">
							<button id="btn_seccion" class="btn btn-success" type="submit" name="option" value="seccion">Buscar</button>
							<button id="reset_seccion" class="btn btn-default" type="button">Restablecer</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Display error -->
	<div id="error_display" class="hidden alert alert-danger">
	</div>

	<!-- Display  result-->
	<div id="result" class="hidden table-padding">
		<table id="table_result" class="table">
			<thead>
				<tr>
					<th>CI</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Grado</th>
					<th>Sección</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>

	<!-- Window modal -->
	<div id="info-modal" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Detalles</h4>
				</div>
				<div class="modal-body">
					<ul class="nav nav-tabs" role="tablist">
						<li class="active" role="presentation">
							<a href="#info-estudiante-tab" data-toggle="tab" role="tab">Estudiante</a>
						</li>
						<li class="" role="presentation">
							<a href="#info-representante-tab" data-toggle="tab" role="tab">Representante</a>
						</li>
					</ul>

					<div class="tab-content" style="padding: 15px">
						<div id="info-estudiante-tab" class="tab-pane active" role="tabpanel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" href="#nombre_completo">Nombre completo</a>
									</h4>
								</div>
								<div id="nombre_completo" class="panel-collapse collapse in">
									<div class="panel-body">
										No traigo nada...
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" href="#nacimiento">Nacimiento</a>
									</h4>
								</div>
								<div id="nacimiento" class="panel-collapse collapse in">
									<div class="panel-body">
										No traigo nada...
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" href="#contacto">Contacto</a>
									</h4>
								</div>
								<div id="contacto" class="panel-collapse collapse in">
									<div class="panel-body">
										No traigo nada...
									</div>
								</div>
							</div>
						</div>

						<div id="info-representante-tab" class="tab-pane" role="tabpanel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" href="#representante_personal">Nombre completo</a>
									</h4>
								</div>
								<div id="representante_personal" class="panel-collapse collapse in">
									<div class="panel-body">
										No traigo nada...
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" href="#representante_contacto">Contacto</a>
									</h4>
								</div>
								<div id="representante_contacto" class="panel-collapse collapse in">
									<div class="panel-body">
										No traigo nada...
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

	<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Edición</h4>
				</div>

				<div class="modal-body">
					<ul class="nav nav-tabs" role="tablist">
						<li class="active" role="presentation">
							<a href="#estudiante-tab" data-toggle="tab" role="tab">Estudiantes</a>
						</li>
						<li role="presentation">
							<a href="#representante-tab" data-toggle="tab" role="tab">Representante</a>
						</li>
						<li role="presentation">
							<a href="#academico-tab" data-toggle="tab" role="tab">Académico</a>
						</li>
					</ul>

					<div class="tab-content" style="padding: 15px">
						<div id="estudiante-tab" class="tab-pane active" role="tabpanel">
							<form id="estudiante_form" class="form-horizontal" action=".html" method="post">

								{!! method_field('PUT') !!}

								<div class="">
									<div class="form-group">
										<label for="ci" class="control-label  col-md-2">C.I.</label>
										<div class="col-md-6">
											<input id="ci" class="form-control" type="number" name="ci" value="">
										</div>
									</div>

									<div class="form-group">

										<label class="contorl-label col-md-2">Nombres</label>
										<div class="">
											<div class="col-md-3">

												<input id="primer_nombre" class="form-control" type="text" name="primer_nombre" value="">
											</div>
											<div class="col-md-3">
												<input id="segundo_nombre" class="form-control" type="text" name="segundo_nombre" value="">
											</div>
											<div class="col-md-3">
												<input id="tercer_nombre" class="form-control" type="text" name="tercer_nombre" value="">
											</div>
										</div>
									</div>

									<div class="form-group">
										{{-- {!! Form::label(NULL, 'Apellidos', ['class' => 'control-label col-md-2']) !!} --}}
										<label class="control-label col-md-2">Apellidos</label>
										<div class="">
											<div class="col-md-5">
												<input id="primer_apellido" class="form-control" type="text" name="primer_apellido" value="">
											</div>
											<div class="col-md-4">
												<input id="segundo_apellido" class="form-control" type="text" name="segundo_apellido" value="">
											</div>
										</div>
									</div>

									<div class="form-group">
										<label for="fecha_nacimiento" class="control-label col-md-2">Fecha de nacimiento</label>
										<div class="">
											<div class="col-md-6">
												<input id="fecha_nacimiento" type="date" name="fecha_nacimiento" class="form-control">
											</div>
										</div>
									</div>

									<div class="form-group">
										<label for="observacion" class="control-label col-md-2">Observación</label>
										<div class="">
											<div class="col-md-9">
												<textarea id="observacion" class="form-control" name="observacion" rows="3"></textarea>
											</div>
										</div>
									</div>

									<div class="form-group">
										{{-- {!! Form::label('direccion', 'Dirección', ['class' => 'control-label col-md-2']) !!} --}}
										<label for="direccion" class="control-label col-md-2">Dirección</label>
										<div class="">
											<div class="col-md-9">
												{{-- {!! Form::textarea('direccion', NULL, ['class' => 'form-control', 'rows' => 3]) !!} --}}
												<textarea id="direccion" class="form-control" name="direccion" rows="3"></textarea>
											</div>
										</div>
									</div>

									<div class="form-group">
										{{-- {!! Form::label(NULL, 'Lugar de nacimiento', ['class' => 'control-label col-md-2']) !!} --}}
										<label class="control-label col-md-2">Lugar de nacimiento</label>
										<div class="col-md-4">
											{{-- {!! Form::select('estado', $estados, NULL, ['class' => 'form-control']) !!} --}}
											<select id="estado" class="form-control" name="estado">
												@foreach($estados as $key => $value)
													<option value="{{$key}}">{{$value}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-4">
											{{-- {!! Form::select('ciudad', ['1', '2'], NULL, ['class' => 'form-control']) !!} --}}
											<select id="ciudad" class="form-control" name="ciudad">

											</select>
										</div>
									</div>
								</div>
							</form>
						</div>

						<div id="representante-tab" class="tab-pane" role="tabpanel">
							{{-- {!! Form::open(['action' => 'EstudianteController@update', 'method' => 'PUT', 'class' => 'form-horizontal']) !!} --}}
							<form id="representante_form" class="form-horizontal" action="#" method="post">
								<div class="">
								{!! method_field('PUT') !!}
								{!! csrf_field() !!}
								<div class="form-group">
									<label for="rep_ci" class="control-label col-md-2">C.I.</label>
									<div class="col-md-6">
										<input id="rep_ci" class="form-control" type="number" name="rep_ci" value="">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-2">Nombres</label>
									<div class="col-md-3">
										<input id="rep_primer_nombre" class="form-control" type="text" name="rep_primer_nombre" value="">
									</div>
									<div class="col-md-3">
										<input id="rep_segundo_nombre" class="form-control" type="text" name="rep_segundo_nombre" value="">
									</div>
									<div class="col-md-3">
										<input id="rep_tercer_nombre" class="form-control" type="text" name="rep_tercer_nombre" value="">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-2">Apellidos</label>
									<div class="col-md-4">
										<input id="rep_primer_apellido" class="form-control" type="text" name="rep_primer_apellido" value="">
									</div>
									<div class="col-md-4">
										<input id="rep_segundo_apellido" class="form-control" type="text" name="rep_segundo_apellido" value="">
									</div>
								</div>

								<div class="form-group">
									<label for="rep_fecha_nacimiento" class="control-label col-md-2">Fecha de nacimiento</label>
									<div class="col-md-6">
										<input id="rep_fecha_nacimiento" class="form-control" type="date" name="rep_fecha_nacimiento" value="">
									</div>
								</div>

								<div class="form-group">
									<label for="rep_direccion" class="control-label col-md-2">Dirección</label>
									<div class="col-md-9">
										<textarea id="rep_direccion" class="form-control" name="rep_direccion" rows="3"></textarea>
									</div>
								</div>

								<div class="form-group">
									<label for="rep_telefono" class="conrol-label col-md-2">Teléfono</label>
									<div class="col-md-4">
										<input id="rep_telefono" class="form-control" type="number" name="rep_telefono" value="">
									</div>

									<label for="rep_telefono_trabajo" class="control-label col-md-2">Trabajo</label>
									<div class="col-md-4">
										<input id="rep_telefono_trabajo" class="form-control" type="number" name="rep_telefono_trabajo" value="">
									</div>
								</div>

								<div class="form-group">
									<label for="rep_trabajo" class="control-label col-md-2">Trabajo</label>
									<div class="col-md-6">
										<input id="rep_trabajo" class="form-control col-md-2" type="text" name="rep_trabajo" value="">
									</div>
								</div>
								</div>

							</form>
						</div>

						<div id="academico-tab" class="tab-pane" role="tabpanel">
							<form id="academico_form" class="form-horizontal" action="#" method="post">
								{!! method_field('PUT') !!}

								<div class="form-group">
									<label class="control-label col-md-2" for="grado">Grado</label>
									<div class="col-md-4">
										<select id="grado" class="form-control grado-select" name="grado">
											@foreach($grados as $key => $value)
												<option value="{{$key}}">{{$value}}</option>
											@endforeach
										</select>
									</div>

									<label class="control-label col-md-2" for="seccion">Sección</label>
									<div class="col-md-4">
										<select id="seccion" class="form-control seccion-select" name="seccion">
										</select>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>

				<div class="modal-footer">
						<button type="submit" class="btn btn-success">Actualizar</button>
						<button type="reset" class="btn btn-default">Restablecer</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{ asset('assets/js/jquery.dataTables.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}" charset="utf-8"></script>
<script>
	$(document).ready(function() {

		// console.log($(this).find("input[name='_token']").val());
		var token = $("input[name='_token'").val(),
			seccion_input = $('#seccion_input'),
			temp = $('select[name=estado]'),
			table_result = undefined;

		for(var i = 0; i < temp.children().length; i++) {
			if(temp.children()[i].value == 10)
				console.log(temp.children()[i]);
		}

		$('#reset_seccion').on('click', function(evento) {
			evento.preventDefault();

			$(':input', '#form_seccion')
				.not(':button, :submit, :reset, :hidden')
				.val('');

			console.log($('#grado').find($('#grado').children()[0]).attr('selected', 'selected'));
			$('#seccion_input').html('');
		});

		// Evento auto-cargado que se activa cuando cambia el select de grado
		$(document).on('change', '.grado-select', function() {
			var value = $(this).val(),
				this_form = $(this).closest('form').find('.seccion-select');

			this_form.html('');

			if(value === 'NULL') {
				return;
			}

			$.post('/secciones/show', {'_token': token, 'grado': value})
			.done(function(data) {
				var newOption = '';

				for(var i = 0; i < data.length; i++) {
					newOption += "<option value='" + data[i].id + "'>" + data[i].nombre + '</option>';
				}

				this_form.html(newOption);
			});
		});

		// Evento de envío de formulario de búsqueda
		$(document).on('submit', '#form_ci, #form_seccion', function(evento) {
			evento.preventDefault();

			var result_div = $('#result');
			var error_display  = $('#error_display');
			var data = $(this).serializeArray();

			// console.log(data)

			$.post('estudiantes/search', data)
			.done(function(resData) {

				var toAdd = '';

				if(resData.success == false) {
					error_display.html('');

					if(!result_div.hasClass('hidden'))
						result_div.addClass('hidden');

					if(error_display.hasClass('hidden'))
						error_display.toggleClass('hidden');

					error_display.append('<span class="glyphicon glyphicon-warning-sign"></span> ' + resData.message);
				}
				else {
					var temp = [];
					if(!error_display.hasClass('hidden'))
						error_display.addClass('hidden')

					for(var i = 0; i < resData.length; i++) {
						// toAdd += '<tr data-ci="' + resData[i].ci + '">';
						// toAdd += '<td id="ci-row">' + resData[i].ci + '</td>';
						// toAdd += '<td>' + resData[i].nombres + '</td>';
						// toAdd += '<td>' + resData[i].apellidos + '</td>';
						// toAdd += '<td>' + resData[i].grado + '</td>';
						// toAdd += '<td>' + resData[i].seccion + '</td>';
						// toAdd += '<td><a class="info-item" href="#" title="Detalles" data-toggle="modal" data-target="#info-modal"><span class="glyphicon glyphicon-info-sign text-primary"></span></a> ';
						// toAdd += '<a class="edit-item" href="#!" title="Editar" data-toggle="modal" data-target="#edit-modal"><span class="glyphicon glyphicon-pencil text-warning"></span></a> ';
						// toAdd += '<a id="delete-item" href="#" title="Eliminar"><span class="glyphicon glyphicon-remove text-danger"></span></a></td>'
						// toAdd += '</tr>';
						resData[i].option = '<a class="info-item" href="#" title="Detalles" data-toggle="modal" data-target="#info-modal"><span class="glyphicon glyphicon-info-sign text-primary"></span></a> ';
						resData[i].option += '<a href="notas/'+ resData[i].id +'" title="Notas" data-toggle="modal"><span class="glyphicon glyphicon-th-list text-success"></span></a> ';
						resData[i].option += '<a href="estudiantes/edit/'+ resData[i].ci +'" title="Editar" data-toggle="modal"><span class="glyphicon glyphicon-pencil text-warning"></span></a> ';
						// resData[i].option += '<a class="edit-item" href="#!" title="Editar" data-toggle="modal" data-target="#edit-modal"><span class="glyphicon glyphicon-pencil text-warning"></span></a> ';
						resData[i].option += '<a class="delete-item" href="estudiantes/delete/'+ resData[i].ci +'" title="Eliminar"><span class="glyphicon glyphicon-remove text-danger"></span></a>';
						temp.push(resData[i]);
					}

					table_result.clear().draw();
					table_result.rows.add(temp).draw();
					// result_div.find('tbody').html('');
					// result_div.find('tbody').append(toAdd);

					if(result_div.hasClass('hidden'))
						result_div.removeClass('hidden');
				}
			})
			.fail(function(data) {
				var message = '';

				$.each(data.responseJSON, function(index, value) {
					message += value + ' ';
				});

				console.log(message);

				error_display.html('');

				if(!result_div.hasClass('hidden'))
					result_div.addClass('hidden');

				if(error_display.hasClass('hidden'))
					error_display.removeClass('hidden');

				error_display.append('<span class="glyphicon glyphicon-warning-sign"></span> ' + message);
			});
		});

		// Evento click en botón de información de estudiante
		$(document).on('click', '.info-item', function(evento) {
			evento.preventDefault();
			var ci = $(this).closest('tr').attr('id');

			console.log(ci);

			$.post('estudiantes', {'_token': token, 'ci': ci})
			.done(function(resData) {
				console.log(resData);

				var info_modal = $('#info-modal');
				var html_modal = {};

				html_modal.nombre_completo = '<strong>C.I.: </strong>'+ resData.ci +'<br>';
				html_modal.nombre_completo += '<strong>Nombre completo: </strong>'+ resData.primer_nombre +' '+ resData.segundo_nombre +' '+ resData.primer_apellido +' '+ resData.segundo_apellido +'<br>';
				html_modal.nombre_completo += '<strong>Sexo: </strong>'+ resData.sexo;
				html_modal.nacimiento = '<strong>Fecha:</strong> '+ resData.fecha_nacimiento +'<br> <strong>Lugar:</strong> '+ resData.ciudad.estado.nombre +'/'+ resData.ciudad.nombre;
				html_modal.contacto = '<strong>Dirección:</strong> '+ resData.direccion;
				html_modal.representante_personal = '<strong>C.I.: </strong>'+ resData.representante.ci;
				html_modal.representante_personal += '<br><strong>Nombre completo: </strong>'+ resData.representante.primer_nombre +' '+ resData.representante.segundo_nombre +' '+ resData.representante.primer_apellido +' '+ resData.representante.segundo_apellido +'<br>';
				html_modal.representante_personal += '<strong>Sexo: </strong>'+ resData.representante.sexo;
				html_modal.representante_contacto = '<strong>Teléfono: </strong>'+ resData.representante.telefono +' / <strong>Trabajo: </strong>'+ resData.representante.telefono_trabajo;
				html_modal.representante_contacto += ' <br><strong>Dirección: </strong>'+ resData.representante.direccion;

				for(var data in html_modal) {
					info_modal.find('#' + data + ' .panel-body').html('');
					info_modal.find('#' + data + ' .panel-body').html(html_modal[data]);
				}

			})
			.fail(function(resData) {
				console.log('fail');
			});
		});

		// Evento click en botón de editar estudiante
		$(document).on('click', '.edit-item', function(evento) {
			evento.preventDefault();

			var ci = $(this).closest('tr').attr('id');

			console.log(ci);

			$.post('estudiantes', {'_token': token, 'ci': ci})
			.done(function(res_data) {
				var estados_select = $('select[name=estado]');
				// console.dir(estados_select.children());

				// console.log(res_data);
				for (var propiedad in res_data) {
					var link = $('#'+ propiedad);

					if (link.length > 0)
						// link.attr('value', res_data[propiedad]);
						link.val(res_data[propiedad]);
					// else {
					// 	var link_textarea = $('#'+ propiedad +'');
					//
					// 	if(link_textarea.length)
					// 		link_textarea.html(res_data[propiedad]);
					// }

					if (propiedad == 'representante' && propiedad != undefined) {
						for (var value in res_data[propiedad]) {
							var link = $('#rep_'+ value);

							if(link.length > 0)
								// link.attr('value', res_data.representante[value]);
								link.val(res_data.representante[value]);
						}
					}
				}

				$('#academico_form .grado-select').children().each(function(index, element) {
					if ($(element).val() == res_data.seccion.grado) {
						$(element).attr('selected', 'selected');
						// $('#academico_form .grado-select').trigger('change');
						return 0;
					}
				});

				$('#estudiante_form #estado').children().each(function(index, element) {
					if ($(element).val() == res_data.ciudad.estado_id) {
						$(element).attr('selected', 'selected');
						// $('#academico_form .grado-select').trigger('change');
						return 0;
					}
				});

				$.post('/secciones/show', {'_token': token, 'grado': res_data.seccion.grado})
				.done(function(response) {
					var newOption = '';

					for(var i = 0; i < response.length; i++) {
						if(res_data.seccion.id == response[i].id)
							newOption += "<option value='" + response[i].id + "' selected='selected'>" + response[i].nombre + '</option>';
						else
							newOption += "<option value='" + response[i].id + "'>" + response[i].nombre + '</option>';
					}

					$('#academico_form .seccion-select').html(newOption);
				})

				$.post('/region/ciudades', {'_token': token, 'estado_id': res_data.ciudad.estado_id})
				.done(function(response) {
					var newOption = '',
						ciudades = response.data;

					for(var i = 0; i < ciudades.length; i++) {
						if(res_data.ciudad.id == ciudades[i].id)
							newOption += "<option value='" + ciudades[i].id + "' selected='selected'>" + ciudades[i].nombre + '</option>';
						else
							newOption += "<option value='" + ciudades[i].id + "'>" + ciudades[i].nombre + '</option>';
					}

					$('#estudiante_form #ciudad').html(newOption);
				})


				for(var i = 0; i < estados_select.children().length; i++)
					if(estados_select.children()[i].value == res_data.ciudad.estado_id)
						console.log(estados_select.children()[i].value);

			});
		});

		// Evento change para cargar ciudades dinámicamente
		$(document).on('change', '#estado', function(evento) {
			evento.preventDefault();
			var estado_id = $(this).val();

			$.post('/region/ciudades', {'_token': token, 'estado_id': estado_id})
			.done(function(response) {
				var newOption = '',
					ciudades = response.data;

				for(var i = 0; i < ciudades.length; i++) {
					newOption += "<option value='" + ciudades[i].id + "'>" + ciudades[i].nombre + '</option>';
				}

				$('#estudiante_form #ciudad').html(newOption);
			})
		})

		// Evento para confirmar la eliminación de un estudiante
		$(document).on('click', '.delete-item', function(evento) {
			evento.preventDefault();

			var confirmar = window.confirm('¿Seguro desea eliminarlo?');

			if(confirmar) {
				window.location = $(this).attr('href');
			}
		});

		// Configuración de DataTable
		if(!($.fn.dataTable.isDataTable('#table_result')))
			table_result = $('#table_result').DataTable({
				// "scrollY":        "150px",
				// "scrollCollapse": true,
				// "paging":         false,
				'info': true,
				"dom": '<"toolbar">frtip',
				'autoWidth': false,
				"columns": [
					{'data': 'ci'},
					{'data': 'nombres'},
					{'data': 'apellidos'},
					{'data': 'grado'},
					{'data': 'seccion'},
					{'data': 'option'}
				],
				'rowId': 'id',
				'language': {
					'search': 'Filtrar:',
					'zeroRecords': 'No existen estudiantes con dicha coincidencia',
					"info": "Mostrando _START_ a _END_ de _TOTAL_ estudiantes",
					'zeroInfo': 'No existen estudiantes registrados',
					'infoEmpty': 'Mostrando 0 a 0 de 0 estudiantes',
					'paginate': {
						'next': 'Siguiente',
						'previous': 'Anterior'
					}
				}
			});
		})
</script>
@endsection
