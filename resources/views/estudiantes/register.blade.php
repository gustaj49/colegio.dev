@extends('layouts.main')

@section('head_content')
	<div class="page-header">
		<h1><span class="glyphicon glyphicon-floppy-disk text-success"></span> Registro de nuevo estudiante</h1>
	</div>
@endsection

@section('content')


<div class="">

	<div class="alert alert-info">
	    {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button> --}}
		<strong>Los marcados con * son campos obligatorios</strong>
	</div>

	<!-- Nav-tabs -->
	<ul class="hidden nav nav-tabs" role="tablist">
		<li class="active" role="presentation">
			<a href="#estudiante-tab" data-toggle="tab" role="tab">Estudiante</a>
		</li>

		<li role="presentation">
			<a href="#representante-tab" data-toggle="tab" role="tab">Representante</a>
		</li>

		<li role="presentation">
			<a href="#academico-tab" data-toggle="tab" role="tab">Académico</a>
		</li>
	</ul>

	<!-- Content-tabs -->
	<form id="estudiante_form" class="form-horizontal form_update" action="{{ action('EstudianteController@store') }}" method="post">
	<div class="tab-content" style="padding: 15px; border-top: 1px solid #ddd;">
		{!! csrf_field() !!}

		<div id="estudiante-tab" class="tab-pane active" role="tabpanel">
			<div class="page-header" style="margin-top: 10px;">
				<h4>Datos de estudiante</h4>
			</div>
			<!-- <form id="estudiante_form" class="form-horizontal form_update" action="{{ action('EstudianteController@update') }}" method="post"> -->

				<div class="">
					<div class="form-group">
						<label for="ci" class="control-label  col-md-2">C.I.*</label>
						<div class="col-md-4">
							<input id="ci" class="form-control" type="number" name="ci" value="" required="required">
						</div>
						<label class="control-label col-md-2">Sexo: *</label>

						<div class="col-md-4">
							<label class="radio-inline">
								<input type="radio" name="sexo" value="Masculino">Masculino
							</label>
							<label class="radio-inline">
								<input type="radio" name="sexo" value="Femenino">Femenino
							</label>
						</div>
					</div>

					<div class="form-group">

						<label class="control-label col-md-2">Nombres*</label>
						<div class="">
							<div class="col-md-3">

								<input id="primer_nombre" class="form-control" type="text" name="primer_nombre" value="" required="required">
							</div>
							<div class="col-md-3">
								<input id="segundo_nombre" class="form-control" type="text" name="segundo_nombre" value="" required="required">
							</div>
							<div class="col-md-3">
								<input id="tercer_nombre" class="form-control" type="text" name="tercer_nombre" value="">
							</div>
						</div>
					</div>

					<div class="form-group">
						{{-- {!! Form::label(NULL, 'Apellidos', ['class' => 'control-label col-md-2']) !!} --}}
						<label class="control-label col-md-2">Apellidos*</label>
						<div class="">
							<div class="col-md-5">
								<input id="primer_apellido" class="form-control" type="text" name="primer_apellido" value="" required="required">
							</div>
							<div class="col-md-4">
								<input id="segundo_apellido" class="form-control" type="text" name="segundo_apellido" value="">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="fecha_nacimiento" class="control-label col-md-2">Fecha de nacimiento*</label>
						<div class="">
							<div class="col-md-6">
								<input id="fecha_nacimiento" type="date" name="fecha_nacimiento" class="form-control" value="" required="required">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="observacion" class="control-label col-md-2">Observación</label>
						<div class="">
							<div class="col-md-9">
								<textarea id="observacion" class="form-control" name="observacion" rows="3"></textarea>
							</div>
						</div>
					</div>

					<div class="form-group">
						{{-- {!! Form::label('direccion', 'Dirección', ['class' => 'control-label col-md-2']) !!} --}}
						<label for="direccion" class="control-label col-md-2">Dirección*</label>
						<div class="">
							<div class="col-md-9">
								{{-- {!! Form::textarea('direccion', NULL, ['class' => 'form-control', 'rows' => 3]) !!} --}}
								<textarea id="direccion" class="form-control" name="direccion" rows="3" required="required"></textarea>
							</div>
						</div>
					</div>

					<div class="form-group">
						{{-- {!! Form::label(NULL, 'Lugar de nacimiento', ['class' => 'control-label col-md-2']) !!} --}}
						<label class="control-label col-md-2">Lugar de nacimiento*</label>
						<div class="col-md-4">
							{{-- {!! Form::select('estado', $estados, NULL, ['class' => 'form-control']) !!} --}}
							<select id="estado" class="form-control" required>
								@foreach($estados as $key => $value)
									<option value="{{ $key }}">{{ $value }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-4">
							{{-- {!! Form::select('ciudad', ['1', '2'], NULL, ['class' => 'form-control']) !!} --}}
							<select id="ciudad" class="form-control" name="ciudad_id" required>
								@foreach($ciudades as $key => $value)
									<option value="{{ $key }}">{{ $value }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="form-group btn-group-center">
					<button type="button" class="next btn btn-success">Siguiente</button>
					<!-- <button type="reset" class="btn btn-default">Restablecer</button> -->
				</div>
			<!-- </form> -->
		</div>

		<div id="representante-tab" class="tab-pane" role="tabpanel">
			<!-- <form id="representante_form" class="form-horizontal form_update" action="{{ action('EstudianteController@update') }}" method="post"> -->
			<div class="page-header" style="margin-top: 10px;">
				<h4>Datos de representante</h4>
			</div>

				<div class="form-group">
					<label for="rep_ci" class="control-label col-md-2">C.I.*</label>
					<div class="col-md-4">
						<input id="rep_ci" class="form-control" type="number" name="rep_ci" value="" required="required">
					</div>
					<label class="control-label col-md-2">Sexo: *</label>

					<div class="col-md-4">
						<label class="radio-inline">
							<input type="radio" name="sexo" value="Masculino" required>Masculino
						</label>
						<label class="radio-inline">
							<input type="radio" name="sexo" value="Femenino">Femenino
						</label>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Nombres*</label>
					<div class="col-md-3">
						<input id="rep_primer_nombre" class="form-control" type="text" name="rep_primer_nombre" value="" required="required">
					</div>
					<div class="col-md-3">
						<input id="rep_segundo_nombre" class="form-control" type="text" name="rep_segundo_nombre" value="" required="required">
					</div>
					<div class="col-md-3">
						<input id="rep_tercer_nombre" class="form-control" type="text" name="rep_tercer_nombre" value="">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Apellidos*</label>
					<div class="col-md-4">
						<input id="rep_primer_apellido" class="form-control" type="text" name="rep_primer_apellido" value="" required="required">
					</div>
					<div class="col-md-4">
						<input id="rep_segundo_apellido" class="form-control" type="text" name="rep_segundo_apellido" value="">
					</div>
				</div>

				<div class="form-group">
					<label for="rep_fecha_nacimiento" class="control-label col-md-2">Fecha de nacimiento*</label>
					<div class="col-md-6">
						<input id="rep_fecha_nacimiento" class="form-control" type="date" name="rep_fecha_nacimiento" value="" required="required">
					</div>
				</div>

				<div class="form-group">
					<label for="rep_direccion" class="control-label col-md-2">Dirección*</label>
					<div class="col-md-9">
						<textarea id="rep_direccion" class="form-control" name="rep_direccion" rows="3" required="required"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label for="rep_telefono" class="control-label col-md-2">Teléfono*</label>
					<div class="col-md-3">
						<input id="rep_telefono" class="form-control" type="number" name="rep_telefono" value="" required="required">
					</div>

					<label for="rep_telefono_trabajo" class="control-label col-md-2">Trabajo</label>
					<div class="col-md-3">
						<input id="rep_telefono_trabajo" class="form-control" type="number" name="rep_telefono_trabajo" value="">
					</div>
				</div>

				<div class="form-group">
					<label for="rep_trabajo" class="control-label col-md-2">Trabajo</label>
					<div class="col-md-6">
						<input id="rep_trabajo" class="form-control col-md-2" type="text" name="rep_trabajo" value="">
					</div>
				</div>

				<div class="form-group btn-group-center">
					<button type="button" class="back btn btn-default">Atrás</button>
					<button type="button" class="next btn btn-success">Siguiente</button>
					<!-- <button type="reset" class="btn btn-default">Restablecer</button> -->
				</div>
			<!-- </form> -->
		</div>

		<div id="academico-tab" class="tab-pane" role="tabpanel">
			<!-- <form id="academico_form" class="form-horizontal form_update" action="#" method="post"> -->
			<div class="page-header" style="margin-top: 10px;">
				<h4>Datos académicos</h4>
			</div>
				<div class="form-group">
					<label class="control-label col-md-2" for="grado">Grado*</label>
					<div class="col-md-4">
						<select id="grado" class="form-control grado-select" name="grado" required>
							@foreach($grados as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>

					<label class="control-label col-md-2" for="seccion">Sección*</label>
					<div class="col-md-4">
						<select id="seccion" class="form-control seccion-select" name="seccion_id" required>
							@foreach ($secciones as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="periodo" class="control-label col-md-2">Periodo a inscribir*</label>
					<div class="col-md-4">
						<select class="form-control" name="periodo_id" required>
							@foreach($periodos as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group btn-group-center">
					<button type="button" class="back btn btn-default">Atrás</button>
					<button type="submit" class="btn btn-primary">Completar</button>
					<!-- <button type="reset" class="btn btn-default">Restablecer</button> -->
				</div>
			<!-- </form> -->
		</div>

	</div>
	</form>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/js/change_estado.js') }}"></script>
<script>
	$(document).ready(function() {
		$(document).on('click', '.next', function(evento) {
			evento.preventDefault();

			$('ul.nav.nav-tabs').children().each(function(index, element) {
				if($(element).hasClass('active')) {
					var next_element = $('ul.nav.nav-tabs').find('li:nth-child('+ parseInt(index+2) +')'),
						required = false;

					$('#'+ $(element).find('a').attr('href').substr(1)).find('*[required]')
					.each(function(index, element) {
						if($(element).val() == '') {

							if(! $(element).closest('div').hasClass('has-error'))
								$(element).closest('div').addClass('has-error');

							$(element).attr({
								'data-toggle': 'tooltip',
								'data-placement': 'top',
								'title': 'Campo obligatorio'
							});

							$(element).tooltip();

							required = true;
							// return false;
						}
					});

					if(required) {
						window.alert('Complete los campos obligatorios antes de avanzar');
						return;
					}

					if($('div.tab-pane.active input[name*=sexo]:checked').val() == undefined) {
						window.alert('No ha seleccionado sexo');
						return;
					}

					if(next_element.length > 0) {
						// Búsqueda y toggle de content-tab
						$('#'+ $(element).find('a').attr('href').substr(1)).toggleClass('active');
						$('#'+ next_element.find('a').attr('href').substr(1)).toggleClass('active');

						// Toggle de nav-tab li
						$(element).toggleClass('active');
						next_element.toggleClass('active');

						return false;
					}
				}
			});
		})
		$(document).on('click', '.back', function(evento) {
			evento.preventDefault();

			$('ul.nav.nav-tabs').children().each(function(index, element) {
				if($(element).hasClass('active')) {
					var next_element = $('ul.nav.nav-tabs').find('li:nth-child('+ index +')');

					if(next_element.length > 0) {
						// Búsqueda y toggle de content-tab
						$('#'+ $(element).find('a').attr('href').substr(1)).toggleClass('active');
						$('#'+ next_element.find('a').attr('href').substr(1)).toggleClass('active');

						// Toggle de nav-tab li
						$(element).toggleClass('active');
						next_element.toggleClass('active');

						return false;
					}
				}
			});
		})
	});
</script>
@endsection
