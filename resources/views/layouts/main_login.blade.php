<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Administración - Control de estudio</title>
	{{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}"> --}}
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
	<link rel="stylesheet" href="{!! asset('assets/css/alertify.css') !!}">
	@yield('custom_css')
</head>
<body>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ url('/') }}">Colegio</a>
		</div>

		</div>
	</div>
</nav>

	<div id="main" class="container">
		@yield('content')
	</div>

<footer>

</footer>
<script src="{{ asset('assets/js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{!! asset('assets/js/alertify.js') !!}"></script>
{{-- <script src="{{ asset('assets/js/jquery-2.2.0.min.js') }}"></script> --}}
{{-- <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script> --}}
@yield('script')
</body>
</html>
