<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Administración - Control de estudio</title>
	{{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}"> --}}
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
	<link rel="stylesheet" href="{!! asset('assets/css/alertify.css') !!}">
	@yield('custom_css')
</head>
<body>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ url('/') }}">Colegio</a>
		</div>

		<div class="navbar-collapse collapse" id="collapmenu">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
						Estudiantes <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('estudiantes/') }}">Consultar</a></li>
						<li><a href="{{ url('estudiantes/register') }}">Registrar</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
						Materias <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('materias/') }}">Consultar</a></li>
						<li><a href="{{ url('materias/register') }}">Registrar</a></li>
					</ul>
				</li>

				@if(Session::get('user.type') == 'admin')
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
						Usuarios <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('usuarios/') }}">Consultar</a></li>
						<li><a href="{{ url('usuarios/register') }}">Registrar</a></li>
					</ul>

				</li>
				@endif
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
						Periodos <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('periodos/') }}">Consultar</a></li>
						<li><a href="{{ url('periodos/register') }}">Registrar</a></li>
					</ul>
				</li>

				{{-- <li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
						Notas <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="/notas">Consultar</a></li>
						<li><a href="/notas/register">Registrar</a></li>
					</ul>
				</li> --}}

				<li><a href="/secciones">Secciones</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" title="">
						{{ Session::get('user.username') }}<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ action('UsuarioController@logout') }}">Cerrar sesión</a></li>
					</ul>
				</li>
			</ul>

		</div>
	</div>
</nav>

	<div id="main" class="container">
		@yield('head_content')

		<div class="aside">
			@if (count($errors) > 0)
					{{-- <ul> --}}
						@foreach ($errors->all() as $error)
							<div class="alert alert-danger">
							{{-- <li> --}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								{{ $error }}
							{{-- </li> --}}
						</div>
						@endforeach
					{{-- </ul> --}}
			@endif

			@if(Session::has('message'))
				<div class="alert alert-{{ session('message')['type'] }}">
					{!! session('message')['msg'] !!}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
		</div>
		
		@yield('content')
	</div>

<footer>

</footer>
<script src="{{ asset('assets/js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{!! asset('assets/js/alertify.js') !!}"></script>
{{-- <script src="{{ asset('assets/js/jquery-2.2.0.min.js') }}"></script> --}}
{{-- <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script> --}}
@yield('script')
</body>
</html>
