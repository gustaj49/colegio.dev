@extends('layouts.main')

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
    <div class="page-header">
        <h1><span class="glyphicon glyphicon-pencil text-warning"></span> Editar {{ $periodo->username }}</h1>
    </div>
@endsection

@section('content')
<div class="">
    <form class="form-horizontal" action="{{ action('PeriodoController@update') }}" method="post">
        {!! csrf_field() !!}

        <input type="hidden" name="id" value="{{ $periodo->id }}">
        <div class="form-group">
            <label class="control-label col-md-2" for="periodo">Periodo</label>
            <div class="col-md-5">
                <input type="text" name="periodo" class="form-control" value="{{ $periodo->periodo }}" required="required">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="inicio">Fecha inicio</label>
            <div class="col-md-5">
                <input type="date" name="inicio" class="form-control" value="{{ $periodo->inicio }}" required="required">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="fin">Fecha fin</label>
            <div class="col-md-5">
                <input type="date" name="fin" class="form-control" value="{{ $periodo->fin }}" required="required">
            </div>
        </div>

        <div class="form-group btn-group-center">
            <button type="submit" class="btn btn-success">Aceptar</button>
            <button type="reset" class="btn btn-default">Restablecer</button>
        </div>
    </form>
</div>
@endsection

@section('javascript')

@endsection
