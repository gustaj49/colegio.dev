@extends('layouts.main')

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
    <div class="page-header">
        <h1><span class="glyphicon glyphicon-floppy-disk text-success"></span> Registro de nuevo periodo</h1>
    </div>
@endsection

@section('content')
<div class="">
    <form id="form_usuario" class="form-horizontal" action="{{ action('PeriodoController@store') }}" method="post">
        {!! csrf_field() !!}

        <div class="form-group">
            <label class="control-label col-md-2" for="periodo">Periodo</label>
            <div class="col-md-5">
                <input type="text" name="periodo" class="form-control" value="" required="required">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="inicio">Fecha inicio</label>
            <div class="col-md-5">
                <input type="date" name="inicio" class="form-control" value="" required="required">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="fin">Fecha fin</label>
            <div class="col-md-5">
                <input type="date" name="fin" class="form-control" value="" required="required">
            </div>
        </div>

        <div class="form-group btn-group-center">
            <button id="" type="submit" class="btn btn-success">Aceptar</button>
            <button type="reset" class="btn btn-default">Restablecer</button>
        </div>
    </form>
</div>
@endsection

@section('script')
<script type="text/javascript">

</script>
@endsection
