@extends('layouts.main')

@section('head_content')
    <div class="page-header" style="text-align: center">
        <h1>Bienvenid@: {{ Session::get('user.username') }}</h1>
    </div>
@endsection

@section('content')
<div class="home">
    <div class="col-md-4">
        <ul class="list-group">
          <li class="list-group-item active">
            Estudiantes
          </li>
          <li class="list-group-item">

            <span class="badge">{{ $estudiante['total'] }}</span>
            Total
          </li>
          <li class="list-group-item">
            <span class="badge">{{ $estudiante['masculino'] }}</span>
            Masculino
          </li>
          <li class="list-group-item">
            <span class="badge">{{ $estudiante['femenino'] }}</span>
            Femenino
          </li>
          {{-- <li class="list-group-item">
            <span class="badge">14</span>
            Mejor promedio
          </li> --}}
          <li class="list-group-item">
            <span class="badge">
                {{ $estudiante['mayor_edad']['age'] }}
            </span>
            Mayor edad
          </li>
          <li class="list-group-item">
            <span class="badge">
                {{ $estudiante['menor_edad']['age'] }}
            </span>
            Menor edad
          </li>
          <li class="list-group-item">
            <span class="badge">
                {{ $estudiante['promedio_edad']['promedio'] }}
            </span>
            Promedio edad
          </li>
        </ul>
    </div>

    <div class="col-md-4">
        <ul class="list-group">
          <li class="list-group-item active">
            Materias
          </li>
          <li class="list-group-item">
            <span class="badge">{{ $materia['total'] }}</span>
            Total
          </li>
          <li class="list-group-item">
            <span class="badge">
                {{ $materia['mayor_grado']['grado'] }}: {{ $materia['mayor_grado']['count_m'] }}
            </span>
            Grado con más
          </li>
          <li class="list-group-item">
            <span class="badge">
                {{ $materia['menor_grado']['grado'] }}: {{ $materia['menor_grado']['count_m'] }}
            </span>
            Grado con menos
          </li>
        </ul>
    </div>

    <div class="col-md-4">
        <ul class="list-group">
          <li class="list-group-item active">
            Notas
          </li>
          <li class="list-group-item">
            <span class="badge">{{ $nota['total'] }}</span>
            Total
          </li>
          <li class="list-group-item">
            <span class="badge">{{ $nota['mayor_diez'] }}</span>
            Mayores de 10
          </li>
          <li class="list-group-item">
            <span class="badge">{{ $nota['menor_diez'] }}</span>
            Menores de 10
          </li>
        </ul>
    </div>
    <div class="col-md-4">
        <ul class="list-group">
          <li class="list-group-item active">
            Secciones
          </li>
          <li class="list-group-item">
            <span class="badge">{{ $seccion['total'] }}</span>
            Total
          </li>
          <li class="list-group-item">
            <span class="badge">
                {{ $seccion['mayor_est']['grado'] }}º {{ $seccion['mayor_est']['nombre'] }}: {{ $seccion['mayor_est']['count_e'] }}
            </span>
            Con más estudiantes
          </li>
          <li class="list-group-item">
            <span class="badge">
                {{ $seccion['menor_est']['grado'] }}º {{ $seccion['menor_est']['nombre'] }}: {{ $seccion['menor_est']['count_e'] }}
            </span>
            Con menos estudiantes
          </li>
        </ul>
    </div>
    <div class="col-md-4">
        <ul class="list-group">
          <li class="list-group-item active">
            Representante
          </li>
          <li class="list-group-item">
            <span class="badge">{{ $representante['total'] }}</span>
            Total
          </li>
          <li class="list-group-item">
            <span class="badge">
                {{ $representante['mayor_est']->count_r }}
            </span>
            Con más estudiantes relacionados
          </li>
          <li class="list-group-item">
            <span class="badge">
                {{ $representante['menor_est']->count_r }}
            </span>
            Con menos estudiantes relacionados
          </li>
        </ul>
    </div>
</div>
@endsection
