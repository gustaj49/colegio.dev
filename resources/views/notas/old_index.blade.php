@extends('layouts.main')

@section('custom_css')
	<link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('content')
<div class="">
    <div class="page-header">
        <h1>Búsqueda de notas</h1>
    </div>

    <div class="aside">
        @if(count($errors))
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        @endif

        @if(Session::has('message'))
            <div class="alert alert-{{ session('message')['type'] }}">
                {!! session('message')['msg'] !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>

    <div class="container-fluid">
        <div class="form-horizontal">
            {!! csrf_field() !!}
            <label class="control-label col-md-1" for="">Grado</label>
            <div class="col-md-3">
                <select id="grado" class="form-control" name="grado">
                    <option value="" selected disabled>Seleccione un grado</option>
                    {{-- @foreach($grados as $key => $value)
                        <option value="{{ $value }}">{{ $key }}</option>
                    @endforeach --}}
                </select>
            </div>
        </div>
        <table id="materias_tabla" class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Grado</th>
                    <th>Opción</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('assets/js/jquery.dataTables.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}" charset="utf-8"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var materias_tabla = undefined;

        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
        });

        $('#grado').on('change', function(evento) {
            evento.preventDefault();
            var grado = $(this).val();

            $.post('materias/materias', {'grado': grado})
            .done(function(response) {
                materias_tabla.clear().draw();

                if(response.status == 'success') {
                    var data = response.data;

                    for (var i = 0; i < data.length; i++) {
                        data[i]['opcion'] = '<a href="materias/edit/'+ data[i].id +'"><span class="glyphicon glyphicon-pencil text-warning"> </span></a> ';
                        data[i]['opcion'] += '<a href="materias/delete/'+ data[i].id +'"><span class="glyphicon glyphicon-remove text-danger"> </span></a>';
                    }

                    materias_tabla.rows.add(data).draw();
                }
            });
        });

        if(!($.fn.dataTable.isDataTable('#materias_tabla'))) {
            materias_tabla = $('#materias_tabla').DataTable({
                'dom': '<"materias_toolbar form-horizontal">frtip',
                'autoWidth': false,
                'columns': [
                    {'data': 'id'},
                    {'data': 'nombre'},
                    {'data': 'grado'},
                    {'data': 'opcion'},
                ],
                'rowId': 'id',
                'language': {
                    'search': 'Filtrar:',
                    'zeroRecords': 'No existen materias para dicho grado',
                    'info': "Mostrando _START_ a _END_ de _TOTAL_ estudiantes",
                    'zeroInfo': 'No ha seleccionado grado'
                }
            });

            $('.materias_toolbar').html('')
        }
    });
</script>
@endsection
