    @extends('layouts.main')

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
    <div class="page-header">
        <h1><span class="glyphicon glyphicon-th-list text-success"></span> Notas: {{ $estudiante->primer_nombre }} {{ $estudiante->primer_apellido }}</h1>
    </div>
@endsection

@section('content')
<div class="">

    {{-- {!! dd($notas) !!} --}}
    @foreach($notas as $key => $nota)
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">{{ $key }}º grado</div>

                <ul class="list-group">
                    @foreach($nota as $value)
                        <li class="list-group-item">
                            @if($value->nota == 'SA')
                                <span class="badge"><abbr title="Sin asignar">{{ $value->nota }}</abbr></span>
                            @else
                                <span class="badge">{{ $value->nota }}</span>
                            @endif
                            {{ $value->nombre }}
                            <a href="{{ url('/notas/edit/'. $estudiante->id .'/'. $value->id) }}"><span class="glyphicon glyphicon-pencil text-primary"></span></a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endforeach
</div>
@endsection

@section('javascript')

@endsection
