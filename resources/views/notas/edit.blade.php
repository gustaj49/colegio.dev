@extends('layouts.main')

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
    <div class="page-header">
        <h1><span class="glyphicon glyphicon-pencil text-warning"></span> Nota: {{ $materia->nombre }} [{{ $materia->grado }}º], {{ $estudiante->primer_nombre }} {{ $estudiante->primer_apellido }}</h1>
    </div>
@endsection

@section('content')
<div class="">
    <form class="form-horizontal" action="{{ action('NotaController@update') }}" method="post">
        {!! csrf_field() !!}

        <input type="hidden" name="materia_id" value="{{ $materia->id }}">
        <input type="hidden" name="estudiante_id" value="{{ $estudiante->id }}">
        <div class="form-group">
            <label class="control-label col-md-2" for="nombre">Nota</label>
            <div class="col-md-5">
                <input type="number" name="nota" class="form-control" value="{{ (! $nota) ? 0 : $nota->nota }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="periodo_id">Periodo</label>
            <div class="col-md-5">
                <select class="form-control" name="periodo_id" required>
                    <option value="" selected disabled>Seleccionar periodo</option>
                    @foreach($periodos as $value)
                        @if($value->id == ((! $nota) ? 0 : $nota->periodo_id))
                            <option title="{{ $value->inicio.' - '.$value->fin }}" value="{{ $value->id }}" selected>{{ $value->periodo }}</option>
                        @else
                            <option title="{{ $value->inicio.' - '.$value->fin }}" value="{{ $value->id }}">{{ $value->periodo }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group btn-group-center">
            <a type="button" href="{{ url('notas/'.$estudiante->id) }}" class="btn btn-default">Volver a notas</a>
            <button type="submit" class="btn btn-primary">Actualizar</button>
            <button type="reset" class="btn btn-default">Restablecer</button>
        </div>
    </form>
</div>
@endsection

@section('javascript')

@endsection
