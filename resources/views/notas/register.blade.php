@extends('layouts.main')

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('content')
<div class="">
    <div class="page-header">
        <h1><span class="glyphicon glyphicon-pencil text-warning"></span> Registro de nueva materia</h1>
    </div>
    <div class="aside">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
        				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        					<span aria-hidden="true">&times;</span>
        				</button>
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('message'))
            <div class="alert alert-{{ session('message')['type'] }}">
                {!! session('message')['msg'] !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
    <div class="">
        <form class="form-horizontal" action="{{ action('MateriaController@store') }}" method="post">
            {!! csrf_field() !!}

            <div class="form-group">
                <label class="control-label col-md-2" for="nombre">Nombre</label>
                <div class="col-md-5">
                    <input type="text" name="nombre" class="form-control" value="">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="nombre">Grado</label>
                <div class="col-md-5">
                    <select class="form-control" name="grado" required>
                        <option value="" selected disabled>Seleccionar grado</option>
                        @foreach($grados as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group btn-group-center">
                <button type="submit" class="btn btn-success">Aceptar</button>
                <button type="reset" class="btn btn-default">Restablecer</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('javascript')

@endsection
