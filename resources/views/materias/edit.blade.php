@extends('layouts.main')

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
    <div class="page-header">
        <h1><span class="glyphicon glyphicon-pencil text-warning"></span> Editar {{ $materia->nombre }}, {{ $materia->grado }}º</h1>
    </div>
@endsection

@section('content')
<div class="">
    <form class="form-horizontal" action="{{ action('MateriaController@update') }}" method="post">
        {!! csrf_field() !!}
        <input type="hidden" name="id" value="{{ $materia->id }}">
        <div class="form-group">
            <label class="control-label col-md-2" for="nombre">Nombre</label>
            <div class="col-md-5">
                <input type="text" name="nombre" class="form-control" value="{{ $materia->nombre }}" required>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="nombre">Grado</label>
            <div class="col-md-5">
                <select class="form-control" name="grado" required>
                    <option value="" selected disabled>Seleccionar grado</option>
                    @foreach($grados as $key => $value)
                        @if($key == $materia->grado)
                            <option value="{{ $key }}" selected>{{ $value }}</option>
                        @else
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group btn-group-center">
            <button type="submit" class="btn btn-primary">Actualizar</button>
            <button type="reset" class="btn btn-default">Restablecer</button>
        </div>
    </form>
</div>
@endsection

@section('javascript')

@endsection
