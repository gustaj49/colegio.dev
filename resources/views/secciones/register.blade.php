@extends('layouts.main')

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
    <div class="page-header">
        <h1><span class="glyphicon glyphicon-floppy-disk text-success"></span> Registro de nueva sección</h1>
    </div>
@endsection

@section('content')
<div class="">
    <form class="form-horizontal" action="{{ action('SeccionController@store') }}" method="post">
        {!! csrf_field() !!}

        <div class="form-group">
            <label class="control-label col-md-2" for="grado">Grado</label>
            <div class="col-md-5">
                <input type="number" name="grado" class="form-control" value="{{ $grado }}" readonly required>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="nombre">Nombre</label>
            <div class="col-md-5">
                <input type="text" name="nombre" class="form-control" value="" required>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="max">Máximo de estudiantes</label>
            <div class="col-md-5">
                <input type="number" name="max" class="form-control" value="" required>
            </div>
        </div>

        <div class="form-group btn-group-center">
            <button type="submit" class="btn btn-success">Aceptar</button>
            <button type="reset" class="btn btn-default">Restablecer</button>
        </div>
    </form>
</div>
@endsection

@section('javascript')

@endsection
