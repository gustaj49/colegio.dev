@extends('layouts.main')

@section('custom_css')
	<link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
	<div class="page-header">
		<h1><span class="glyphicon glyphicon-list-alt text-primary"></span> Listado de secciones</h1>
	</div>
@endsection

@section('content')
<div class="">
	@foreach($secciones as $key => $nota)
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
					<a href="{{ url('/secciones/register/'. $key) }}" style="display: inline; float: right"><span style="color: white" class="glyphicon glyphicon-plus"></span></a>
					{{ $key }}º grado
				</div>

                <ul class="list-group">
                    @foreach($nota as $value)
                        <li class="list-group-item">
                            <span class="badge">{{ $value->max }}</span>
                            {{ $value->nombre }}
                            <a style="margin-left: 10px" href="{{ url('/secciones/edit/'. $value->id ) }}"><span class="glyphicon glyphicon-pencil text-primary"></span></a>
                            <a class="delete-item" href="{{ url('/secciones/delete/'. $value->id ) }}"><span class="glyphicon glyphicon-remove text-danger"></span></a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endforeach
</div>
@endsection

@section('script')
<script src="{{ asset('assets/js/jquery.dataTables.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}" charset="utf-8"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var materias_tabla = undefined;

        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
        });

		$(document).on('click', '.delete-item', function (evento) {
			evento.preventDefault();
			var confirmar = window.confirm('¿Seguro desea eliminar la sección?');

			if (confirmar)
				window.location = $(this).attr('href');

		});

        $('#grado').on('change', function(evento) {
            evento.preventDefault();
            var grado = $(this).val();

            $.post('materias/materias', {'grado': grado})
            .done(function(response) {
                materias_tabla.clear().draw();

                if(response.status == 'success') {
                    var data = response.data;

                    for (var i = 0; i < data.length; i++) {
                        data[i]['opcion'] = '<a href="materias/edit/'+ data[i].id +'"><span class="glyphicon glyphicon-pencil text-warning"> </span></a> ';
                        data[i]['opcion'] += '<a href="materias/delete/'+ data[i].id +'"><span class="glyphicon glyphicon-remove text-danger"> </span></a>';
                    }

                    materias_tabla.rows.add(data).draw();
                }
            });
        });

        if(!($.fn.dataTable.isDataTable('#materias_tabla'))) {
            materias_tabla = $('#materias_tabla').DataTable({
                'dom': '<"materias_toolbar form-horizontal">frtip',
                'autoWidth': false,
                'columns': [
                    {'data': 'id'},
                    {'data': 'nombre'},
                    {'data': 'grado'},
                    {'data': 'opcion'},
                ],
                'rowId': 'id',
                'language': {
                    'search': 'Filtrar:',
                    'zeroRecords': 'No existen materias para dicho grado',
                    'info': "Mostrando _START_ a _END_ de _TOTAL_ estudiantes",
                    'zeroInfo': 'No ha seleccionado grado'
                }
            });

            $('.materias_toolbar').html('')
        }
    });
</script>
@endsection
