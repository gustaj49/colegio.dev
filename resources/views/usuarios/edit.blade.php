@extends('layouts.main')

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
    <div class="page-header">
        <h1><span class="glyphicon glyphicon-pencil text-warning"></span> Editar {{ $usuario->username }}</h1>
    </div>
@endsection

@section('content')
<div class="">
    <div class="alert alert-info">
        <strong>Los campos de contraseña y repetir contraseña puede dejarlos en blanco en caso de no desear cambiarlas</strong>
    </div>
    <form id="form_usuario" class="form-horizontal" action="{{ action('UsuarioController@update') }}" method="post">
        {!! csrf_field() !!}

        <input type="hidden" name="id" value="{{ $usuario->id }}">
        <div class="form-group">
            <label class="control-label col-md-2" for="username">Usuario</label>
            <div class="col-md-5">
                <input type="text" name="username" class="form-control" value="{{ $usuario->username }}" required>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="email">Correo electrónico</label>
            <div class="col-md-5">
                <input type="email" name="email" class="form-control" value="{{ $usuario->email }}" required>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="password">Contraseña</label>
            <div class="col-md-5">
                <input type="password" name="password" class="form-control" value="">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="repeat_password">Repetir contraseña</label>
            <div class="col-md-5">
                <input type="password" name="repeat_password" class="form-control" value="">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="type">Privilegio</label>
            <div class="col-md-5">
                <select class="form-control" name="type" required>
                    <option value="" disabled>Seleccionar privilegio</option>
                    @foreach(['user' => 'Usuario', 'admin' => 'Admininistrador'] as $key => $value)
                        @if($key == $usuario->type)
                            <option value="{{ $key }}" selected>{{ $value }}</option>
                        @endif
                        <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group btn-group-center">
            <button type="submit" class="btn btn-success">Aceptar</button>
            <button type="reset" class="btn btn-default">Restablecer</button>
        </div>
    </form>
</div>
@endsection

@section('script')
<script type="text/javascript">
$('#form_usuario').on('submit', function(evento) {

    var password = $('input[name=password]').val(),
    repeat_password = $('input[name=repeat_password]').val();

    if(password != repeat_password) {
        evento.preventDefault();
        window.alert('Las contraseñas no coinciden');
        return;
    }

    $('#form_usuario').trigger('submit');

});
</script>
@endsection
