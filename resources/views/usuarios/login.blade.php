@extends('layouts.main_login')

@section('custom_css')
<style media="screen">


.box_login {
	margin: 5% auto;
	/* padding: 0 100px; */
	width: 100%;
	height: 100%;
	border-radius: 4%;
	max-width: 450px;
	max-height: 450px;
	/* width: 50%; */
}

#img_banner {
	display: block;
	margin: auto;
	max-width: 25%;
	max-height: 25%;
}

.title {
	text-align: center;
}

#tabla table {
    border: 2px solid #DDDDDD;
}

.form-box-center {
	margin: 5% auto;
	padding: 0px auto;
}

.input-login {
	margin: 25px auto;
}


</style>
@endsection

@section('content')
<div class="">
    <div id="" class="jumbotron box_login">
        <div class="banner">
            <img src="{{ asset('assets/images/login_icon.png') }}" id="img_banner">
        </div>
		<div class="aside">
			@if(count($errors))
				@foreach($errors->all() as $error)
					{{ $error }}
				@endforeach
			@endif

			@if(Session::has('message'))
				<div style="margin-top: 10px" class="alert alert-{{ session('message')['type'] }}">
					{!! session('message')['msg'] !!}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
		</div>
        <!-- <h2 class="form-signin">Ingreso</h2> -->
        <form id="form-login" action="{{ action('UsuarioController@login') }}" method="post" accept-charset="utf-8">
			{!! csrf_field() !!}
			{{-- {{ dd(Session::all()) }} --}}
			<div id="username-group" class="input-login form-group">
                <!-- <label class="control-label"></label> -->
                <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <input class="form-control" type="text" name="username" autofocus="1" placeholder="Nombre de usuario" required>
                </div>

            </div>

            <div id="password-group" class="input-login form-group">
                <!-- <label class="control-label"></label> -->
                <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                    <input class="form-control" type="password" name="password" placeholder="Contraseña" required>
                </div>

            </div>

            <div style="text-align: center;">
                <input id="ingresar" class="btn" type="submit" name="" value="Ingresar" style="background-color: #0095DA; color: white">
                <input class="btn btn-default" type="reset" name="" value="Restablecer">
            </div>
        </form>
    </div>
</div>
@endsection
