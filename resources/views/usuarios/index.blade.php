@extends('layouts.main')

@section('custom_css')
	<link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" media="screen" title="no title" charset="utf-8">
@endsection

@section('head_content')
	<div class="page-header">
		<h1><span class="glyphicon glyphicon-list-alt text-primary"></span> Listado de usuarios</h1>
	</div>
@endsection

@section('content')
<div class="container-fluid">
    <table id="usuarios_tabla" class="table">
        <thead>
            <tr>
                <th>Usuario</th>
                <th>Correo</th>
                <th>Tipo</th>
                <th>Opción</th>
            </tr>
        </thead>
        <tbody>
			@foreach($usuarios as $key => $value)
				<tr id="{{ $value->id }}">
					<td>{{ $value->username }}</td>
					<td>{{ $value->email }}</td>
					<td>{{ ($value->type == 'user') ? 'Usuario' : 'Admininistrador' }}</td>
					<td>
						<a href="{{ url('usuarios/edit/'. $value->id) }}"><span class="glyphicon glyphicon-pencil text-warning"></span></a>
						<a class="delete-item" href="{{ url('usuarios/delete/'. $value->id) }}"><span class="glyphicon glyphicon-remove text-danger"></span></a>
					</td>
				</tr>
			@endforeach
        </tbody>
    </table>
</div>
@endsection

@section('script')
<script src="{{ asset('assets/js/jquery.dataTables.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}" charset="utf-8"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var usuarios_tabla = undefined;

        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
        });

		// Evento para confirmar la eliminación de un estudiante
		$(document).on('click', '.delete-item', function(evento) {
			evento.preventDefault();

			var confirmar = window.confirm('¿Seguro desea eliminarlo?');

			if(confirmar) {
				window.location = $(this).attr('href');
			}
		});

        if(!($.fn.dataTable.isDataTable('#usuarios_tabla'))) {
            materias_tabla = $('#usuarios_tabla').DataTable({
                // 'dom': '<"materias_toolbar form-horizontal">frtip',
                'autoWidth': false,
                'columns': [
                    {'data': 'username'},
                    {'data': 'email'},
                    {'data': 'type'},
                    {'data': 'opcion'},
                ],
                'language': {
                    'search': 'Filtrar:',
                    'zeroRecords': 'No existen usuarios registrados con dicha coincidencia',
                    'info': "Mostrando _START_ a _END_ de _TOTAL_ usuarios",
                    'zeroInfo': 'No existen usuarios registrados',
					'infoEmpty': 'Mostrando 0 a 0 de 0 usuarios',
					'lengthMenu': 'Mostrar _MENU_',
					'paginate': {
						'next': 'Siguiente',
						'previous': 'Anterior'
					}
                }
            });
        }
    });
</script>
@endsection
