$(document).ready(function() {
	// Evento auto-cargado que se activa cuando cambia el select de estado
	$('#estado').on('change', function(evt) {
		evt.preventDefault();
		var token = $("input[name='_token'").val();
		var req_data = {'estado_id': $(this).val(), '_token': token};

		$.post('/region/ciudades', req_data)
		.done(function(res_data) {
			var ciudades = res_data.data,
				ciudades_html = '';

			for(var i = 0; i < ciudades.length; i++) {
				ciudades_html += '<option value='+ ciudades[i].id +'>' + ciudades[i].nombre + '</option>';
			}

			$('#ciudad').html(ciudades_html);
		});
	});
	// Evento auto-cargado que se activa cuando cambia el select de grado
	$(document).on('change', '.grado-select', function() {
		var value = $(this).val(),
			token = $(this).closest('form').find('input[name=_token]').val(),
			this_form = $(this).closest('form').find('.seccion-select');

		this_form.html('');

		if(value === 'NULL') {
			return;
		}

		$.post('/secciones/show', {'_token': token, 'grado': value})
		.done(function(data) {
			var newOption = '';

			for(var i = 0; i < data.length; i++) {
				newOption += "<option value='" + data[i].id + "'>" + data[i].nombre + '</option>';
			}

			this_form.html(newOption);
		});
	});

})