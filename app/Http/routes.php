<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome', ['title' => 'Colegio.dev']);
});*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
    Route::get('/login', 'UsuarioController@login');
    Route::post('/login', 'UsuarioController@sesion');

    Route::group(['middleware' => 'session'], function() {
        Route::get('/', 'WelcomeController@index');

        // Routes Estudiantes
        Route::group(['prefix' => 'estudiantes'], function() {
            Route::get('/', 'EstudianteController@index');
            Route::post('/', 'EstudianteController@details');
            Route::post('/search', 'EstudianteController@search');
            Route::get('/edit/{id}', 'EstudianteController@edit')->where(['id' =>  '[0-9]+']);
            Route::put('/update', 'EstudianteController@update');
            Route::get('/register', 'EstudianteController@register');
            Route::post('/store', 'EstudianteController@store');
            Route::get('/delete/{id}', 'EstudianteController@delete')->where(['id' =>  '[0-9]+']);
        });

        Route::group(['prefix' => 'materias'], function() {
            Route::get('/', 'MateriaController@index');
            Route::post('/materias', 'MateriaController@materias')->where(['id' => '[0-9]+']);
            Route::get('/register', 'MateriaController@register');
            Route::post('/store', 'MateriaController@store');
            Route::get('/edit/{id}', 'MateriaController@edit')->where(['id' =>  '[0-9]+']);
            Route::post('/update', 'MateriaController@update');
            Route::get('/delete/{id}', 'MateriaController@delete')->where(['id' =>  '[0-9]+']);
        });

        Route::group(['prefix' => 'notas'], function() {
            Route::get('/{id}', 'NotaController@index')->where('id', '[0-9]+');
            Route::get('/register', 'NotaController@register');
            Route::get('/edit/{id}/{materia}', 'NotaController@edit')->where(['id' => '[0-9]+', 'nota' => '[0-9]+']);
            Route::post('/update', 'NotaController@update');
        });

        Route::group(['prefix' => 'periodos'], function() {
            Route::get('/', 'PeriodoController@index');
            Route::get('/register', 'PeriodoController@register');
            Route::post('/store', 'PeriodoController@store');
            Route::get('/edit/{id}', 'PeriodoController@edit')->where(['id' => '[0-9]+']);
            Route::post('/update', 'PeriodoController@update');
            Route::get('/delete/{id}', 'PeriodoController@delete')->where(['id' => '[0-9]+']);
        });

        Route::group(['prefix' => 'secciones'], function() {
            Route::get('/', 'SeccionController@index');
            Route::post('/show', 'SeccionController@show');
            Route::get('/register/{grado}', 'SeccionController@register')->where('grado', '[0-9]+');
            Route::get('/edit/{id}', 'SeccionController@edit')->where('id', '[0-9]+');
            Route::get('/register/{id}', 'SeccionController@store')->where('id', '[0-9]+');
            Route::post('/store', 'SeccionController@store');
            Route::post('/update', 'SeccionController@update');
            Route::get('/delete/{id}', 'SeccionController@delete')->where('id', '[0-9]+');
        });

        Route::group(['prefix' => 'usuarios'], function() {
            Route::get('/', 'UsuarioController@index');
            Route::get('/register', 'UsuarioController@register');
            Route::get('/edit/{id}', 'UsuarioController@edit')->where('id', '[0-9]+');
            Route::post('/update', 'UsuarioController@update');
            Route::get('/delete/{id}', 'UsuarioController@delete')->where('id', '[0-9]+');
            Route::post('/store', 'UsuarioController@store');
            Route::get('/logout', 'UsuarioController@logout');
        });

        Route::post('region/ciudades', 'RegionController@selectCiudades');
    });
