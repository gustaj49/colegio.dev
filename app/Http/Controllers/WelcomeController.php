<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\EstudianteRepository;
use App\Repositories\MateriaRepository;
use App\Repositories\NotaRepository;
use App\Repositories\SeccionRepository;

use App\Estudiante;
use App\Representante;
use App\Seccion;
use App\Materia;
use App\Nota;

class WelcomeController extends Controller
{
    private $estudiante;
    private $materia;
    private $nota;
    private $seccion;

    public function __construct(EstudianteRepository $estudiante, MateriaRepository $materia,
                                NotaRepository $nota, SeccionRepository $seccion) {
         $this->estudiante = $estudiante;
         $this->materia = $materia;
         $this->nota = $nota;
         $this->seccion = $seccion;
    }

    public function index() {
        $estadisticas = [
            'estudiante' => [
                'total' => Estudiante::all()->count(),
                'masculino' => Estudiante::where('sexo', 'masculino')->count(),
                'femenino' => Estudiante::where('sexo', 'femenino')->count(),
                'mayor_edad' => Estudiante::select(DB::raw('YEAR(CURDATE())-YEAR(fecha_nacimiento) as age'))
                    ->orderBy('age', 'DESC')->first(),
                'menor_edad' => Estudiante::select(DB::raw('YEAR(CURDATE())-YEAR(fecha_nacimiento) as age'))
                    ->orderBy('age', 'ASC')->first(),
                'promedio_edad' => Estudiante::select(DB::raw('AVG(YEAR(CURDATE())-YEAR(fecha_nacimiento)) as promedio'))
                    ->first(),
            ],
            'materia' => [
                'total' => Materia::all()->count(),
                'mayor_grado' => Materia::select(DB::raw('count(*) as count_m'), 'grado')
                    ->groupBy('grado')->orderBy('grado', 'asc')->first(),
                'menor_grado' => Materia::select(DB::raw('count(*) as count_m'), 'grado')
                    ->groupBy('grado')->orderBy('grado', 'desc')->first(),
            ],
            'seccion' => [
                'total' => Seccion::all()->count(),
                'mayor_est' =>
                    Estudiante::select(DB::raw('count(*) as count_e'), 'nombre', 'grado', 'seccion_id')
                    ->join('secciones', 'secciones.id', '=', 'estudiantes.seccion_id')
                    ->groupBy('seccion_id')->orderBy('seccion_id', 'ASC')->first(),
                'menor_est' =>
                    Estudiante::select(DB::raw('count(*) as count_e'), 'nombre', 'grado', 'seccion_id')
                    ->join('secciones', 'secciones.id', '=', 'estudiantes.seccion_id')
                    ->groupBy('seccion_id')->orderBy('seccion_id', 'DESC')->first(),
            ],
            'nota' => [
                'total' => Nota::all()->count(),
                'mayor_diez' => Nota::where('nota', '>=', '10')->count(),
                'menor_diez' => Nota::where('nota', '<', '10')->count()
            ],
            'representante' => [
                'total' => Representante::all()->count(),
                'mayor_est' => DB::table('estudiantes')->select('representantes.primer_nombre',
                        DB::raw('count(*) as count_r'), 'representantes.id')
                    ->join('representantes', 'representantes.id', '=', 'estudiantes.representante_id')
                    ->groupBy('representantes.id')
                    ->orderBy('count_r', 'DESC')
                    ->first(),
                'menor_est' => DB::table('estudiantes')->select('representantes.primer_nombre',
                        DB::raw('count(*) as count_r'), 'representantes.id')
                    ->join('representantes', 'representantes.id', '=', 'estudiantes.representante_id')
                    ->groupBy('representantes.id')
                    ->orderBy('count_r', 'ASC')
                    ->first()
            ]
        ];

        return view('index', $estadisticas);
    }
}
