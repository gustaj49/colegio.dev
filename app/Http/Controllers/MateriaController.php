<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\MateriaRepository;
use App\Repositories\SeccionRepository;

class MateriaController extends Controller
{

    private $materia;
    private $seccion;

    public function __construct(MateriaRepository $materia, SeccionRepository $seccion) {
        $this->materia = $materia;
        $this->seccion = $seccion;
    }

    public function index() {

        return view('materias.index', ['grados' => $this->seccion->selectAllGrado()]);

    }

    public function materias(Request $request) {
        if($request->isMethod('post') && $request->ajax() && $request->has('grado')) {
            $grado = $request->input('grado');

            $materias = $this->materia->getByGrado($grado);

            if(count($materias) > 0)
                return response()->json(['status' => 'success', 'data' => $materias]);
            else {
                return response()->json(['status' => 'fail']);
            }
        }
    }

    public function register() {
        return view('materias.register', ['grados' => $this->seccion->selectAllGrado()]);
    }

    public function store(Request $request) {
        if($request->isMethod('post')) {
            $this->validate($request, [
                'nombre' => 'required|string',
                'grado' => 'required',
            ]);

            if($this->materia->store(['nombre' => $request->input('nombre'), 'grado' => $request->input('grado')]));
                \Session::flash('message', ['type' => 'success', 'msg' => 'Materia agregada con éxito']);

                // url('materias/register');
                return back();
        }
    }

    public function edit($id) {
        $datos = $this->materia->getById($id);

        return view('materias.edit', ['materia' => $datos, 'grados' => $this->seccion->selectAllGrado()]);
    }

    public function update(Request $request) {
        if($request->isMethod('post') && $request->has('id')) {
            $this->validate($request, [
                'id' => 'required|integer',
                'nombre' => 'required|string',
                'grado' => 'required'
            ]);

            $result = $this->materia->update($request->except('_token'));

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Materia actualizada con éxito']);

                return back();
            }
        }
    }

    public function delete($id) {
        if(is_numeric($id)) {
            $result = $this->materia->delete($id);

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Materia eliminada con éxito']);
                return back();
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Materia no eliminada']);
                return back();
            }
        }
    }
}
