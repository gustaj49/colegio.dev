<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use \App\Repositories\SeccionRepository;

class SeccionController extends Controller
{
    private $seccion;

   	public function __construct(SeccionRepository $repo_seccion)
   	{
   		$this->seccion = $repo_seccion;
   	}

   	public function show(Request $request)
   	{
   		if($request->isMethod('post') && $request->has('grado') && $request->ajax())
        {
            return response()->json($this->seccion->getByGrado($request->input('grado')));
        }
   	}

    public function index() {
        $datos = $this->seccion->getAllSeccion();
        $secciones = [];

        // dd($datos);

        foreach ($datos as $key => $value) {
            $grado = $value->grado;

            if(array_key_exists($grado, $secciones)) {
                $secciones[$grado][] = $value;
            } else {
                $secciones[$grado] = [];
                $secciones[$grado][] = $value;
            }
        }

        return view('secciones.index', ['secciones' => $secciones]);
    }

    public function edit($id) {
        $seccion = $this->seccion->getById($id);
        $grados = $this->seccion->selectAllGrado();

        return view('secciones.edit', ['seccion' => $seccion, 'grados' => $grados]);
    }

    public function register($grado) {

        return view('secciones.register', ['grado' => $grado]);
    }

    public function update(Request $request) {
        if ($request->isMethod('post') && $request->has('id')) {
            $before_update = $this->seccion->getById($request->input('id'));
            $data = $request->only(['id', 'grado', 'max']);
            $data['nombre'] = strtoupper($request->input('nombre'));

            $this->validate($request, [
                'id' => 'required',
                'nombre' => 'required|string|max:1',
                'grado' => 'required',
                'max' => 'required',
            ]);

            if ($before_update->nombre != $data['nombre'])
                if (! $this->seccion->checkNombre($data)) {
                    \Session::flash('message', ['type' => 'danger', 'msg' => 'La combinación de nombre y grado ya existen']);
                    return back();
                }

            $result = $this->seccion->update($data);

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Sección actualizada con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Sección no actualizada']);
            }

            return back();
        }
    }

    public function store (Request $request) {
        if ($request->isMethod('post') && $request->has('grado')) {
            $data = $request->only(['grado', 'max']);
            $data['nombre'] = strtoupper($request->input('nombre'));

            $this->validate($request, [
                'nombre' => 'required|string|max:1',
                'grado' => 'required',
                'max' => 'required',
            ]);

            if (! $this->seccion->checkNombre($data)) {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'La combinación de nombre y grado ya existen']);
                return back();
            }

            $result = $this->seccion->store($data);

            if ($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Sección creada con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Sección no creada']);
            }

            return back();
        }
    }

    public function delete ($id) {
        if (is_numeric($id)) {
            $result = $this->seccion->delete($id);

            if ($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Sección eliminada con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Sección no eliminada']);
            }

            return back();
        }
    }

}
