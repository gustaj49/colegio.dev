<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use \App\Repositories\RegionRepository;

class RegionController extends Controller
{
	private $region;

	public function __construct(RegionRepository $region)
	{
		$this->region = $region;
	}

    public function selectCiudades(Request $request)
    {
    	if($request->isMethod('post') && $request->has('estado_id'))
    	{
    		$this->validate($request, [
    			'estado_id' => 'required|numeric'
    		]);

    		$data = $this->region->getCiudadesByEstado($request->input('estado_id'));

    		return response()->json($data);
    	}
    }
}
