<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UsuarioRepository;
use Hash;

class UsuarioController extends Controller
{
    private $usuario;

    public function __construct(UsuarioRepository $usuario) {
        $this->usuario = $usuario;
    }

    public function index() {
        $usuarios = $this->usuario->getAll();

        return view('usuarios.index', ['usuarios' => $usuarios]);
    }

    public function register() {

        return view('usuarios.register');
    }

    public function login() {
        if(\Session::has('user'))
            return redirect('/');
        else
            return view('usuarios.login');
    }

    public function logout(Request $request) {
        $request->session()->forget('user');

        return back();
    }

    public function sesion(Request $request) {
        if($request->isMethod('post')) {
            $this->validate($request, [
                'username' => 'required',
                'password' => 'required',
            ]);
            $usuario = $this->usuario->getByUsername($request->only(['username']));

            if(count($usuario) > 0) {
                if(Hash::check($request->input('password'), $usuario->password)) {
                    // dd($usuario);
                    $request->session()->put('user', [
                        'id' => $usuario->id,
                        'username' => $usuario->username,
                        'type' => $usuario->type,
                    ]);
                    return redirect('/');
                }
            }

            \Session::flash('message', ['type' => 'danger', 'msg' => 'Datos de acceso incorrectos']);
            return back();
        }
    }

    public function edit($id) {
        $usuario = $this->usuario->getById($id);

        return view('usuarios.edit', ['usuario' => $usuario]);
    }

    public function update(Request $request) {
        if($request->isMethod('post') && $request->has('id')) {
            $before_update = $this->usuario->getById($request->input('id'));

            $validation = [
                'id' => 'required|exists:usuarios',
                'username' => 'required|string|unique:usuarios,username,'.$before_update->id,
                'email' => 'required|email|unique:usuarios,email,'.$before_update->id,
                'type' => 'required|in:user,admin'
            ];
            $data = $request->only(['id', 'username', 'email', 'type']);

            if($request->has('password')) {
                $validation['password'] = 'required|string|min:6';
                $data['password'] = \Hash::make($request->input('password'));
            }

            $this->validate($request, $validation);

            $result = $this->usuario->update($data);

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Usuario actualizado con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Usuario no actualizado']);
            }

            return back();
        }
    }

    public function delete($id) {
        if(is_numeric($id)) {
            $result = $this->usuario->delete($id);

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Usuario eliminado con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Usuario no eliminado']);
            }

            return back();
        }
    }

    public function store (Request $request) {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'username' => 'required|string|unique:usuarios',
                'email' => 'required|email|unique:usuarios',
                'password' => 'required|string|min:6',
                'type' => 'required|in:user,admin',
            ]);

            $data = $request->only(['username', 'email', 'type']);
            $data['password'] = \Hash::make($request->input('password'));

            $result = $this->usuario->store($data);

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Usuario registrado con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Usuario no registrado']);
            }

            return back();
        }
    }
}
