<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\EstudianteRepository;
use App\Repositories\NotaRepository;
use App\Repositories\MateriaRepository;
use App\Repositories\PeriodoRepository;

class NotaController extends Controller
{
    private $estudiante;
    private $nota;
    private $periodo;
    private $materia;

    public function __construct(EstudianteRepository $estudiante, NotaRepository $nota,
                                MateriaRepository $materia, PeriodoRepository $periodo) {
        $this->estudiante = $estudiante;
        $this->nota = $nota;
        $this->materia = $materia;
        $this->periodo = $periodo;
    }

    public function index($ci) {
        $ci = intval($ci);
        $datos = $this->nota->getWithAllGrado($ci);
        $notas = [];

        foreach ($datos as $key => $value) {
            $grado = $value->grado;

            if(array_key_exists($grado, $datos)) {
                $notas[$grado][] = $value;
            } else {
                $notas[$grado] = [];
                $notas[$grado][] = $value;
            }
        }

        return view('notas.index', [
            'estudiante' => $this->estudiante->getById($ci), 'notas' => $notas]);
    }

    public function edit($id, $materia_id) {
        $materia = $this->materia->getById($materia_id);
        $estudiante = $this->estudiante->getById($id);
        $nota = $this->nota->getByCiMateria(['id' => $id, 'materia' => $materia_id]);
        $periodos = $this->periodo->getAll();

        return view('notas.edit', ['materia' => $materia, 'estudiante' => $estudiante,
                                'periodos' => $periodos, 'nota' => $nota]);
    }

    public function update(Request $request) {
        if($request->isMethod('post')) {
            $this->validate($request, [
                'nota' => 'required|numeric',
                'estudiante_id' => 'required|exists:estudiantes,id',
                'materia_id' => 'required|exists:materias,id',
                'periodo_id' => 'required|exists:periodos,id',
            ]);
            $data = $request->only(['nota', 'estudiante_id', 'materia_id', 'periodo_id']);
            $result = $this->nota->update($data);

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Materia actualizada con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Materia no actualizada ']);
            }
            return back();
        }
    }


}
