<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PeriodoRepository;

class PeriodoController extends Controller
{
    private $periodo;

    public function __construct(PeriodoRepository $periodo) {
        $this->periodo = $periodo;
    }

    public function index() {
        $periodos = $this->periodo->getAll();

        return view('periodos.index', ['periodos' => $periodos]);
    }

    public function register() {

        return view('periodos.register');
    }

    public function store(Request $request) {
        if($request->isMethod('post')) {
            $this->validate($request, [
                'periodo' => 'required',
                'inicio' => 'required|date',
                'fin' => 'required|date',
            ]);

            $result = $this->periodo->store($request->only(['periodo', 'inicio', 'fin']));

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Periodo creado con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Periodo no creado']);
            }
            return back();
        }
    }

    public function delete($id) {
        if(is_numeric($id)) {
            $result = $this->periodo->delete($id);

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Periodo eliminado con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Periodo no eliminado']);
            }
            return back();
        }
    }

    public function edit($id) {
        $id = intval($id);
        $periodo = $this->periodo->getById($id);

        return view('periodos.edit', ['periodo' => $periodo]);
    }

    public function update(Request $request) {
        if($request->isMethod('post') && $request->has('id')) {

            $this->validate($request, [
                'id' => 'required|exists:periodos,id',
                'periodo' => 'required',
                'inicio' => 'required',
                'fin' => 'required',
            ]);

            $result = $this->periodo->update($request->only(['id', 'periodo', 'inicio', 'fin']));

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Periodo actualizado con éxito']);
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Periodo no actualizado']);
            }
            return back();
        }
    }
}
