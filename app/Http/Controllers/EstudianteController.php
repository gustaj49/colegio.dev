<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use \App\Estudiante;
use \App\Repositories\RegionRepository;
use \App\Repositories\SeccionRepository;
use \App\Repositories\EstudianteRepository;
use \App\Repositories\RepresentanteRepository;

class EstudianteController extends Controller
{
    private $seccion;
    private $region;
    private $estudiante;
    private $representante;

    public function __construct(RegionRepository $region, SeccionRepository $seccion,
                                EstudianteRepository $estudiante, RepresentanteRepository $representante)
    {
        $this->region = $region;
        $this->seccion = $seccion;
        $this->estudiante = $estudiante;
        $this->representante = $representante;
    }

    public function index()
    {
        $estados = $this->region->selectAllEstados();

        $grados = ['NULL' => 'Seleccionar'];
        $grados = $this->seccion->selectAllGrado();

        return view('estudiantes.index', ['grados' => $grados, 'estados' => $estados]);
    }

    public function details(Request $request)
    {

        $this->validate($request, [
            'ci' => 'required|numeric'
        ]);

        $data = Estudiante::where('id', $request->input('ci'))->first();
        $data->ciudad->estado;
        $data->seccion;
        $data->representante;

    	return response()->json(collect($data));
    }

    public function search(Request $request)
    {
        if($request->isMethod('post') && $request->has('option') && $request->ajax())
        {
            $est = [];
            $data = [];

            if($request->input('option') === 'ci')
            {
                $this->validate($request, [
                    'ci_search' => 'required|numeric'
                ]);

                $data[] = Estudiante::where('ci', $request->input('ci_search'))->first();
            }
            elseif($request->input('option') === 'seccion')
            {
                $this->validate($request, [
                    'seccion_input' => 'required|numeric'
                ]);

                $data = Estudiante::where('seccion_id', $request->input('seccion_input'))->get();
                // echo json_encode(empty($data)); return;
            }

            // var_dump($data->isEmpty()); return;

            if($request->input('option') == 'seccion' && ($data->isEmpty()))
                $est = ['success' => FALSE, 'message' => 'No existen estudiantes para dicha seccion'];
            if($request->input('option') == 'ci' && ($data[0] == NULL))
                $est = ['success' => FALSE, 'message' => 'Estudiante no encontrado'];
            else
                foreach($data as $temp)
                    {
                        $est[] = [
                            'id' => $temp->id,
                            'ci' => $temp->ci,
                            'nombres' => $temp->primer_nombre.' '.$temp->segundo_nombre,
                            'apellidos' => $temp->primer_apellido.' '.$temp->segundo_apellido,
                            'grado' => $temp->seccion->grado,
                            'seccion' => $temp->seccion->nombre
                        ];
                    }

            return response()->json($est);
        }
    }

    public function edit(Request $request, $ci)
    {
        $datos = $this->estudiante->getByCi($ci);

        if(empty($datos))
            abort(404);

        $datos->seccion;
        $datos->ciudad;
        $datos->periodo;
        $periodos = [];
        $estados = [];
        $grados = [];
        $secciones = [];
        $ciudades = $this->region->selectCiudades($datos->ciudad->estado_id);

        foreach($this->seccion->getAllGrado() as $value)
            $grados[$value->grado] = $value->grado;

        foreach($this->seccion->getByGrado($datos->seccion->grado) as $value)
            $secciones[$value->id] = $value->nombre;

        foreach(\App\Estado::all() as $estado)
            $estados[$estado->id] = $estado->nombre;

        foreach(\App\Periodo::all() as $periodo)
            $periodos[$periodo->id] = $periodo->periodo;


        return view('estudiantes.edit', ['estudiante' => $datos,
                                        'representante' => $datos->representante,
                                        'estados' => $estados,
                                        'grados' => $grados,
                                        'secciones' => $secciones,
                                        'ciudades' => $ciudades,
                                        'periodos' => $periodos]);
    }

    public function update(Request $request)
    {
        if($request->isMethod('PUT') && $request->has('option'))
        {
            $status = '';
            $option = $request->input('option');

            if($option == 'estudiante' || $option == 'all')
            {
                $this->validate($request, [
                    'ci' => 'required|unique:estudiantes,ci,'.$request->input('id'),
                    'primer_nombre' => 'required|string',
                    'segundo_nombre' => 'required|string',
                    'tercer_nombre' => 'string',
                    'primer_apellido' => 'required|string',
                    'segundo_apellido' => 'string',
                    'fecha_nacimiento' => 'required|date|string',
                    'observacion' => 'string',
                    'direccion' => 'required|string',
                    'ciudad_id' => 'required|numeric|exists:ciudades,id',
                ]);

                $update = [];

                foreach ($request->all() as $key => $value) {
                    if ($key != 'option' && $key != '_token' && $key != '_method')
                        $update[$key] = $value;
                }

                $result = $this->estudiante->update($update, $request->input('ci'));

                if($result) {
                    $status = ['type' => 'success', 'msg' => 'Estudiante actualizado con éxito'];
                } else {
                    $status = ['type' => 'danger', 'msg' => 'Estudiante no actualizado'];
                }
            }
            if($option == 'representante' || $option == 'all')
            {
                $this->validate($request, [
                    'rep_ci' => 'required',
                    'rep_primer_nombre' => 'required|string',
                    'rep_segundo_nombre' => 'required|string',
                    'rep_tercer_nombre' => 'string',
                    'rep_primer_apellido' => 'required|string',
                    'rep_segundo_apellido' => 'string',
                    'rep_fecha_nacimiento' => 'required|date',
                    'rep_telefono' => 'required',
                ]);

                $data = $request->all();
                $update = [];

                foreach ($data as $key => $value) {
                    $new_key = explode('_', $key, 2);
                    if (count($new_key) > 1 && !empty($new_key[0])) {
                        $new_key = $new_key[1];
                        $update[$new_key] = $value;
                    }
                }

                $estudiante = $this->estudiante->getByCi($request->input('ci'));
                $result = $this->representante->update($update, $estudiante->representante->ci);

                if($result) {
                    $status = ['type' => 'success', 'msg' => 'Representante actualizado con éxito'];
                } else {
                    $status = ['type' => 'danger', 'msg' => 'Representante no actualizado'];
                }

            }
            if($option == 'academico' || $option == 'all')
            {
                $this->validate($request, [
                    'grado' => 'required',
                    'seccion' => 'required',
                    'periodo' => 'required',
                ]);

                $update = [
                    'seccion_id' => $request->input('seccion'),
                    'periodo_id' => $request->input('periodo'),
                ];

                $result = $this->estudiante->update($update, $request->input('ci'));

                if($result) {
                    $status = ['type' => 'success', 'msg' => 'Estudiante actualizado con éxito'];
                } else {
                    $status = ['type' => 'danger', 'msg' => 'Estudiante no actualizado'];
                }
            }

            \Session::flash('message', $status);

            // $request->session()->flash('message', $status['msg']);
            return back();
            // return response()->json($status);
        }
    }

    public function register() {
        //echo 'asdasdf';
        $periodos = [];
        $estados = [];
        $grados = $this->seccion->selectAllGrado();
        $secciones = [];
        $ciudades = $this->region->selectCiudades(1);

        foreach($this->seccion->getByGrado(1) as $value)
            $secciones[$value->id] = $value->nombre;

        foreach(\App\Estado::all() as $estado)
            $estados[$estado->id] = $estado->nombre;

        foreach(\App\Periodo::all() as $periodo)
            $periodos[$periodo->id] = $periodo->periodo;

        return view('estudiantes.register', ['estados' => $estados,
            'grados' => $grados,
            'secciones' => $secciones,
            'ciudades' => $ciudades,
            'periodos' => $periodos]);
    }

    public function store(Request $request) {

        if($request->isMethod('post')) {
            $estudiate = [];
            $representante = [];
            $data = [];

            // dd($request->input('sexo'));

            $this->validate($request, [
                'ci' => 'required',
                'sexo' => 'required|string',
                'primer_nombre' => 'required|string',
                'segundo_nombre' => 'required|string',
                'tercer_nombre' => 'string',
                'primer_apellido' => 'required|string',
                'segundo_apellido' => 'string',
                'sexo' => 'string|in:Masculino,Femenino',
                'fecha_nacimiento' => 'required|date|string',
                'observacion' => 'string',
                'direccion' => 'required|string',
                'ciudad_id' => 'required|numeric|exists:ciudades,id',
                'rep_primer_nombre' => 'required|string',
                'rep_segundo_nombre' => 'required|string',
                'rep_tercer_nombre' => 'string',
                'rep_primer_apellido' => 'required|string',
                'rep_segundo_apellido' => 'string',
                'rep_sexo' => 'string|in:Masculino,Femenino',
                'rep_fecha_nacimiento' => 'required|date',
                'rep_telefono' => 'required',
                'rep_telefono_trabajo' => 'string',
                'rep_trabajo' => 'string',
                // 'grado' => 'required|numeric',
                'seccion_id' => 'required|numeric',
                'periodo_id' => 'required|numeric',
            ]);
            $data = $request->all();

            $max_seccion = $this->seccion->getById($request->input('seccion_id'))->max;
            $cant_actual = Estudiante::where('seccion_id', $request->input('seccion_id'))->count();


            if($max_seccion == $cant_actual) {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Seccion ya esta completa']);
                return back();
            }

            foreach ($data as $key => $value) {
                if(substr_count($key, 'rep') > 0) {
                    $representante[explode('rep_', $key)[1]] = $value;
                } else {
                    if($key != '_token')
                    $estudiante[$key] = $value;
                }
            }

            if($this->representante->store($representante)) {
                // print_r($this->representante->getByCi($representante['ci']));
                $estudiante['representante_id'] = $this->representante->getByCi($representante['ci'])->id;
                $result = $this->estudiante->store($estudiante);

                if($result == 'Exist') {
                    \Session::flash('message', ['type' => 'danger', 'msg' => 'El estudiante ya está inscrito']);
                    return back();
                } elseif($result) {
                    \Session::flash('message', ['type' => 'success', 'msg' => 'Estudiante inscrito con éxito']);
                    return back();
                }
            }
        }

        \Session::flash('message', 'Estudiante no inscrito');
        back();
    }

    public function delete(Request $request, $ci) {
        if(is_numeric($ci)) {
            $result = $this->estudiante->delete($ci);

            if($result) {
                \Session::flash('message', ['type' => 'success', 'msg' => 'Estudiante eliminado con éxito']);
                return redirect('/estudiantes');
            } else {
                \Session::flash('message', ['type' => 'danger', 'msg' => 'Estudiante no eliminado']);
                return back();
            }
        }
    }

}
