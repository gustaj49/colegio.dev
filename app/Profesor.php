<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $table = 'profesores';

    public function materias()
    {
    	return $this->belongsToMany(Materia::class, 'profesores_materias');
    }
}
