<?php 
namespace App\Repositories;

Abstract class Repository {

	abstract public function store();
	abstract public function update();
	abstract public function delete();

	public function statusResult($result = NULL)
	{
		if($result != NULL && $result->count() > 0)
			return ['status' => TRUE, 'data' => $result];

		return ['status' => FALSE];
	}
}

?>