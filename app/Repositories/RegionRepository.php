<?php

namespace App\Repositories;

use \App\Repositories\Repository;
use \App\Estado;
use \App\Ciudad;

class RegionRepository extends Repository {

	public function getCiudadesByEstado($estado_id = NULL)
	{
		return $this->statusResult(Estado::find($estado_id)->ciudades);
	}

	public function selectCiudades($estado_id = NULL) 
	{
		$ciudades = [];

		foreach (Estado::find($estado_id)->ciudades()->get() as $value) {
		// foreach (Ciudad::where('estado_id', $estado_id)->get()->toArray() as $key => $value) {
			$ciudades[$value->id] = $value->nombre;
		}

		return $ciudades;
	}

	public function store() {}
	public function update() {}
	public function delete() {}

	public function selectAllEstados()
	{
		$estados = [];

		foreach(Estado::orderBy('nombre', 'asc')->get() as $estado)
		{
			$estados[$estado->id] = $estado->nombre;
		}

		return $estados;
	}

}

?>
