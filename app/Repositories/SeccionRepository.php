<?php

namespace App\Repositories;

use \App\Seccion;

class SeccionRepository
{
	public function getByGrado($grado = 0)
	{
		return Seccion::where('grado', $grado)->get(['id', 'nombre']);
	}

	public function getAllGrado()
	{
		return Seccion::groupBy('grado')->get(['grado']);
	}

	public function selectAllGrado()
	{
		$grados = [];

		foreach($this->getAllGrado()->toArray() as $value)
            $grados[$value['grado']] = $value['grado'];

        return $grados;
	}

	public function getAllSeccion() {
		return Seccion::orderBy('nombre')->get();
	}

	public function getById($id = NULL) {
		if($id != NULL) {
			return Seccion::find($id);
		}

		return FALSE;
	}

	public function checkNombre ($data = NULL) {
		if ($data != NULL) {
			$result = Seccion::where(['nombre' => $data['nombre'], 'grado' => $data['grado']])->get();

			if (count($result) == 0)
				return TRUE;
		}

		return FALSE;
	}

	public function update ($data = NULL) {
		if ($data != NULL) {
			$seccion = Seccion::where('id', $data['id']);
			$result = $seccion->update($data);

			if ($result) {
				return TRUE;
			}
		}
		return FALSE;
	}

	public function store ($data = NULL) {
		if ($data != NULL) {
			$result = Seccion::create($data);

			if ($result) {
				return TRUE;
			}
		}
		return FALSE;
	}

	public function delete ($id = NULL) {
		if ($id != NULL) {
			$seccion = Seccion::where('id', $id);
			$result = $seccion->delete();

			if ($result) {
				return TRUE;
			}
		}
		return FALSE;
	}
}

?>
