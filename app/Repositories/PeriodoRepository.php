<?php

namespace App\Repositories;

use App\Periodo;

class PeriodoRepository
{

	public function getAll() {
		return Periodo::all();
	}

	public function getById($id = NULL) {
		if($id != NULL) {
			return Periodo::find($id);
		}

		return FALSE;
	}

	public function update($data = NULL) {
		if($data != NULL) {
			$periodo = Periodo::where('id', $data['id']);
			return $periodo->update($data);
		}

		return FALSE;
	}

	public function delete($id = NULL) {
		if($id != NULL) {
			$periodo = Periodo::where('id', $id);
			return $periodo->delete();
		}

		return FALSE;
	}

	public function store ($data = NULL) {
		if ($data != NULL) {
			return Periodo::create($data);
		}

		return FALSE;
	}

}


?>
