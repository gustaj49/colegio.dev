<?php

namespace App\Repositories;

use \App\Estudiante;

class EstudianteRepository
{
	public function getAll()
	{
		return Estudiante::all();
	}

	public function getByCi($ci = 0)
	{
		return Estudiante::where('ci', $ci)->first();
	}

	public function getById($id = 0)
	{
		return Estudiante::find($id);
	}

	public function getBySeccion($seccion = 0)
	{
		return Estudiante::where('seccion_id', $seccion)->first();
	}

	public function getOnly($id)
	{
		return Estudiante::findOrFail($id);
	}

	public function sendData($data)
	{
		return ['success' => $success, 'data' => $data];
	}

	public function update($data = NULL, $ci = NULL)
	{
		if($data != NULL && $ci != NULL) {
			$estudiante = Estudiante::where('ci', $ci);

			return $estudiante->update($data);
		}
	}

	public function store($data = NULL) {
		if($data != NULL) {
			if(count($this->getByCi($data['ci']))) {
				return 'Exist';
			}

			return Estudiante::create($data);
		}
	}

	public function delete($ci = NULL) {
		if($ci != NULL) {
			return Estudiante::where('ci', $ci)->delete();
		}
	}

}


?>
