<?php
namespace App\Repositories;

use App\Materia;

class MateriaRepository {

    public function getByGrado($grado = NULL) {
        if($grado != NULL) {
            return Materia::where('grado', $grado)->get();
        }

        return FALSE;
    }

    public function store($data = NULL) {
        if($data != NULL) {
            // $materia = new Materia;
            // $materia->fill($data);
            // return $materia->save();
            return Materia::create($data);
        }
        return FALSE;
    }

    public function getById($id = NULL) {
        if($id != NULL) {
            return Materia::find($id);
        }
        return FALSE;
    }

    public function update($data = NULL) {
        if($data != NULL) {
            $materia = Materia::find($data['id']);
            $materia->fill($data);
            return $materia->save();
            // return Materia::update($data);
        }

        return FALSE;
    }

    public function delete($id = NULL) {
        if($id != NULL) {
            return Materia::where('id', $id)->delete();
        }
    }

}
?>
