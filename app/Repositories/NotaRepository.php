<?php
namespace App\Repositories;

use App\Nota;
use DB;

class NotaRepository {
    private $temp;

    public function getWithAllGrado($ci = NULL) {
        if($ci != NULL) {
            $this->temp = $ci;
            $result = DB::table('materias')
                ->select('materias.id', DB::raw('IFNULL(notas.nota, "SA") as nota'), 'materias.nombre',
                    'materias.grado')
                ->leftJoin('notas', function($join) {
                    $join->on('materias.id', '=', 'notas.materia_id')
                    ->where('notas.estudiante_id', '=', $this->temp);
                })
                ->leftJoin('estudiantes', 'estudiantes.id', '=', 'notas.estudiante_id')
                ->orderBy('materias.grado')
                ->get();

            unset($this->temp);
            return $result;
        }

        return FALSE;
    }

    public function getByCiMateria($data = NULL) {
        if($data != NULL) {
            $result = Nota::where(['estudiante_id' => $data['id'], 'materia_id' => $data['materia']])
                        // ->leftJoin('estudiantes', 'estudiantes.id', '=', 'notas.estudiante_id')
                        // ->leftJoin('materias', 'materias.id', '=', 'notas.materia_id')
                        ->first();

            if(count($result) > 0) {
                return $result;
                // dd($result);
            }
        }

        return FALSE;
    }

    public function update($data = NULL) {
        if($data != NULL) {
            $result = Nota::updateOrCreate(
                    ['estudiante_id' => $data['estudiante_id'], 'materia_id' => $data['materia_id']],
                    $data);

            if(count($result) > 0)
                return TRUE;
        }
        return FALSE;
    }
}

?>
