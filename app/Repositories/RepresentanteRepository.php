<?php
namespace App\Repositories;

use \App\Representante;

class RepresentanteRepository {

	public function update($data = NULL, $ci = NULL) {
		if($data != NULL && $ci != NULL) {
			$representante = Representante::where('ci', $ci);

			return $representante->update($data);
		}

		return FALSE;
	}

	public function store($data = NULL) {
		if($data != NULL) {
			// $representante = Representante::firstOrNew(array('ci' => $data['ci']));
			$representante = Representante::firstOrNew(array('ci' => $data['ci']));
			$representante->fill($data)->save();

			return $representante;
		}
	}

	public function getByCi($ci = NULL) {
		if($ci != NULL) {
			return Representante::where('ci', $ci)->first();
		}
	}

}

?>