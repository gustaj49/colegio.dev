<?php

namespace App\Repositories;

use App\Usuario;

class UsuarioRepository
{

	public function getAll() {
		return Usuario::all();
	}

	public function getById($id = NULL) {
		if($id != NULL) {
			return Usuario::find($id);
		}

		return FALSE;
	}

	public function update($data = NULL) {
		if($data != NULL) {
			$usuario = Usuario::where('id', $data['id']);
			return $usuario->update($data);
		}

		return FALSE;
	}

	public function delete($id = NULL) {
		if($id != NULL) {
			$usuario = Usuario::where('id', $id);
			return $usuario->delete();
		}

		return FALSE;
	}

	public function store ($data = NULL) {
		if ($data != NULL) {
			return Usuario::create($data);
			// $usuario = new Usuario();
			// $usuario->fill($data);
			// dd($usuario);
			// return $usuario->save();
		}

		return FALSE;
	}

	public function getByUsername($data = NULL) {
		if($data != NULL) {
			return Usuario::where('username', $data['username'])->first();
		}
		return FALSE;
	}

}


?>
