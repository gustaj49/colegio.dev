<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representante extends Model
{
    //
    // protected $visible = ['ci'];
    protected $fillable = ['ci', 'primer_nombre', 'segundo_nombre', 'tercer_nombre', 'primer_apellido', 
    					'segundo_apellido', 'fecha_nacimiento', 'sexo', 'direccion', 'telefono',
    					'telefono_trabajo', 'trabajo'];

    public function estudiantes()
    {
    	return $this->hasMany(Estudiante::class);
    }
}
