<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    //
    protected $fillable = ['nombre', 'grado'];
    public $timestamps = false;

    public function notas()
    {
    	return $this->hasMany(Nota::class);
    }

    public function profesores()
    {
    	return $this->belongsToMany(Profesor::class, 'profesores_materias');
    }
}
