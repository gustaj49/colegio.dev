<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $fillable = ['id', 'nota', 'estudiante_id', 'materia_id', 'periodo_id'];
    public $timestamps = false;

    public function estudiante()
    {
    	return $this->belongsTo(Estudiante::class);
    }

    public function materia()
    {
    	return $this->belongsTo(Materia::class);
    }

    public function periodo()
    {
    	return $this->belongsTo(Nota::class);
    }

    public function seccion()
    {
    	return $this->belongsTo(Seccion::class);
    }
}
