<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
    //
    protected $table = 'secciones';
    protected $visible = ['id', 'nombre', 'grado', 'max'];
    protected $fillable = ['id', 'nombre', 'grado', 'max'];
    public $timestamps = false;

    public function estudiantes()
    {
    	return $this->hasMany(Estudiante::class);
    }

    public function notas()
    {
    	return $this->hasMany(Notas::class);
    }
}
