<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{

    protected $fillable = ['ci', 'primer_nombre', 'segundo_nombre', 'tercer_nombre', 'primer_apellido', 
					'segundo_apellido', 'fecha_nacimiento', 'sexo', 'direccion', 'anio_egreso', 'observacion',
					'ciudad_id', 'periodo_id', 'seccion_id', 'representante_id'];

	public function seccion()
	{
		return $this->belongsTo(Seccion::class);
	}

	public function notas()
	{
		return $this->hasMany(Nota::class);
	}

	public function ciudad()
	{
		return $this->belongsTo(Ciudad::class, 'ciudad_id');
	}

	public function representante()
	{
		return $this->belongsTo(Representante::class, 'representante_id');
	}

	public function periodo()
	{
		return $this->belongsTo(Periodo::class);
	}

}
