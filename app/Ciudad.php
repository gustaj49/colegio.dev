<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudades';
    public $timestamps = false;
    // protected $visible = ['id'];

    public function estado()
    {
    	return $this->belongsTo(Estado::class);
    }

    public function estudiantes()
    {
    	return $this->hasMany(Estudiante::class);
    }
}
