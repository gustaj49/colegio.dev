<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
	public $timestamps = false;
	protected $fillable = ['periodo', 'inicio', 'fin'];

    public function notas()
    {
    	return $this->hasMany(Nota::class);
    }

    public function estudiantes()
    {
    	return $this->hasMany(Estudiante::class);
    }
}
